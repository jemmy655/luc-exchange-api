package ltd.trilobite.algorithm.miner.dao;

import java.sql.ResultSet;

public interface IResultCallBack {
    void exe(ResultSet rs);
}
