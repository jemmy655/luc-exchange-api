package ltd.trilobite.algorithm.miner;

import ltd.trilobite.algorithm.miner.dao.entry.MemberStatus;
import ltd.trilobite.algorithm.miner.dao.entry.Person;
import org.apache.commons.collections.map.HashedMap;

import java.util.Map;

public class AppPool {
    public static Map<Long, MemberStatus> MemberStatusPool=new HashedMap();
    public static Map<Long, Person> PersonPool=new HashedMap();
    public static  Long PersonUpdateId=0l;
    public static Long MemberStatusUpdateId=0l;
}
