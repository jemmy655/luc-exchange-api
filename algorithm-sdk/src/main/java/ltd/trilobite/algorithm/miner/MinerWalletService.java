package ltd.trilobite.algorithm.miner;

import ltd.trilobite.algorithm.miner.dao.MinerWalletDao;
import ltd.trilobite.algorithm.miner.dao.PersonMinerProducteDao;
import ltd.trilobite.algorithm.miner.dao.entry.MinerWallet;
import ltd.trilobite.algorithm.miner.dao.entry.PersonMinerProducte;

import java.util.ArrayList;
import java.util.List;

/**
 * 矿机钱包业务逻辑
 */
public class MinerWalletService {

    PersonMinerProducteDao personMinerProducteDao=new PersonMinerProducteDao();
    MinerWalletDao minerWalletDao=new MinerWalletDao();

    /**
     * 根据配置新增矿机产出
     * @param personId
     * @param personMinerId
     */
    public void addForMinerProduct(Long personId,Long personMinerId){

        PersonMinerProducte param=new PersonMinerProducte();
        param.setPersonId(personId);
        param.setPersonMinerId(personMinerId);
        //获取产出列表
        List<PersonMinerProducte> lists= personMinerProducteDao.list(param,PersonMinerProducte.class);
        List<Object> saves=new ArrayList<>();
        for(PersonMinerProducte row:lists){
            MinerWallet minerWallet=new MinerWallet();
            minerWallet.setRewardsTypeId(row.getRewardsTypeId());
            minerWallet.setNum(row.getHourVal());
            minerWallet.setPersonMinerId(row.getPersonMinerId());
            saves.add(minerWallet);
        }

        minerWalletDao.adds(saves);//保存到矿机钱包
        personMinerProducteDao.updateAlready_val(personMinerId);//更新已经产出的矿量
    }
}
