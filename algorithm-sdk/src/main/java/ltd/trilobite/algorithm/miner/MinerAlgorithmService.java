package ltd.trilobite.algorithm.miner;

import ltd.trilobite.algorithm.miner.dao.PersonMinerDao;
import ltd.trilobite.algorithm.miner.dao.PersonMinerPowerDao;
import ltd.trilobite.algorithm.miner.dao.RewardsBillDao;
import ltd.trilobite.algorithm.miner.dao.entry.PersonMiner;

import java.io.File;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Calendar;


/**
 * 矿机算法服务
 */
public class MinerAlgorithmService {
    PersonMinerPowerDao personMinerPowerDao=new PersonMinerPowerDao();
    MinerWalletService minerWalletService=new MinerWalletService();
    PersonMinerDao personMinerDao=new PersonMinerDao();
    RewardsBillDao  rewardsBillDao=new RewardsBillDao();

    public  int getHour(){
        Calendar calendar=Calendar.getInstance();
        return calendar.get(Calendar.HOUR_OF_DAY);
    }

    /**
     * 更新矿机状态,当状态为激活的时候
     * @param personMiner
     * @return
     */
    private PersonMiner mineState(PersonMiner personMiner,int hour){
        //PersonMiner result=new PersonMiner();
        //当矿机使用时间超出或等于可运行时间，矿机为完成状态
       // Calendar calendar=Calendar.getInstance();
        //int hour=calendar.get(Calendar.HOUR_OF_DAY);
        if(personMiner.getIsActive()==3){
            return personMiner;
        }
        int power=getPower(personMiner.getPersonId())-hour;
        //晚上0 点的时候，尝试进行激活
        if(hour==0){
            personMiner.setIsActive(2);
        }
        if(power<=1){
            System.out.println("暂停:编号:"+personMiner.getMinerId()+"当前小时:"+hour);
            personMiner.setIsActive(1);
        }
        if(power==1&&hour==23){
            personMiner.setIsActive(2);
        }


        if(personMiner.getUseHour()>=personMiner.getMaxRunHour()){
            System.out.println("设置当前矿机完成");
            personMiner.setIsActive(3);
        }
        return personMiner;
    }

    /**
     * 运行矿机
     * @param hour
     */
    public void runMiner(int hour){
        personMinerDao.runActiveMiner((personMiner)->{
            PersonMiner param=mineState(personMiner,hour);
            outputOre(param);//矿机产出逻辑
            param.setWorkTime(Timestamp.from(Instant.now()));
            personMinerDao.update(param);
            if (hour==0){
                //在奖励钱包中扣除消耗货币的能量
                rewardsBillDao.minePowerConsum(personMiner.getPersonId());
            }
            if(hour==23){
                //清除能量
                //System.out.println("清除能量");
                //在奖励钱包中扣除消耗货币的能量

                personMinerPowerDao.delete(personMiner.getPersonId());

            }
        });

    }

    /**
     * 运行矿机之后所执行的事件
     */
    public void runMinerAfter(){

    }


    /**
     * 当矿机激活状态的时候产出矿
     * @param personMiner
     */
    private boolean outputOre(PersonMiner personMiner){
        //产出矿
        if(personMiner.getIsActive()==2){
            int use=personMiner.getUseHour()+1;
            System.out.println("矿机编号:"+personMiner.getMinerId()+"已使用"+use+"小时");
            personMiner.setUseHour(use);
            System.out.println("执行产出逻辑");
            minerWalletService.addForMinerProduct(personMiner.getPersonId(),personMiner.getPersonMinerId());
        }

        return false;
    }





    /**
     * 获取矿机能量
     * @return
     */
    private Integer getPower(Long PersonId){
       // if(PersonId==92) {
            return personMinerPowerDao.queryPower(PersonId);
        //}else
         //   return 8;
    }

    /**
     * 每天第一次拉取可用矿机
     */
    public void pullPersonMiner(){

    }



    /**
     * 延长时间
     */
    public void timeExpand(){

    }


}
