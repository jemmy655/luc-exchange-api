package ltd.trilobite.algorithm.person;

import ltd.trilobite.algorithm.miner.AppPool;
import ltd.trilobite.algorithm.miner.dao.MemberStatusDao;
import ltd.trilobite.algorithm.miner.dao.PersonDao;
import ltd.trilobite.algorithm.miner.dao.entry.MemberStatus;
import ltd.trilobite.algorithm.miner.dao.entry.Person;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashSet;
import java.util.Set;

public class PersonService {
    MemberStatusDao memberStatusDao=new MemberStatusDao();
    Logger log = LogManager.getLogger(PersonService.class);
    /**
     * 获取上级人员信息
     */
    public Person getPrvInfo(Long personId){
       MemberStatus memberStatus= AppPool.MemberStatusPool.get(personId);
       if(memberStatus==null){
           return null;
       }
       return getInfo(memberStatus.getParentId());
    }

    /**
     * 每次注册后需要做的事情
     * @param personId
     * @param parentId
     */
    public void reisgerAfter(Long personId,Long parentId){

        //1、计算下级人员团队
        MemberStatus param=new MemberStatus();
        //param.setMemberStatusId();
        //memberStatusDao.updateParents(personId);
        memberStatusDao.updateNextTeam(personId);
        //2、同步用户信息
        //3、创建聊天组
        //4、加入父亲会员的聊天组
    }

    /**
     * 获取人员信息
     * @param personId
     * @return
     */
    public Person getInfo(Long personId){
        return AppPool.PersonPool.get(personId);
    }

    /**
     * 运行所有的会员结构
     * @param iMemberStatusCallBack
     */
    public void runAllMemberStatus(IMemberStatusCallBack iMemberStatusCallBack){
        memberStatusDao.runMemberAll(iMemberStatusCallBack);
    }

    /**
     * 向上运行会员结构
     * @param iLevelCallback
     */
    public void runMemberParent(ILevelCallback iLevelCallback,Long personId){
        memberStatusDao.runMemberParent(iLevelCallback,personId);
    }

    public void loop(Set<Long> set,Long personId){
        Person person=getPrvInfo(personId);
        set.add(person.getPersonId());
        if(person!=null) {
            if(person.getPersonId()!=0) {
                loop(set, person.getPersonId());
            }
        }
    }


}
