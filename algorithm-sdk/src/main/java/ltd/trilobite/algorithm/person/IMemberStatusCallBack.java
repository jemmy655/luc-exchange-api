package ltd.trilobite.algorithm.person;

import ltd.trilobite.algorithm.miner.dao.entry.MemberStatus;



public interface IMemberStatusCallBack {
    void execute(MemberStatus memberStatus);
}
