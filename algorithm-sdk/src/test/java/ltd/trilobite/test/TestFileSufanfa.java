package ltd.trilobite.test;

import org.h2.store.fs.FileUtils;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class TestFileSufanfa {
    @Test
    public void insertFile(){
        File file=new File("/opt/mytestdir");
        for(int i=0;i<1000000;i++){
            try {
                new File(file.getAbsolutePath()+"/"+i+".text").createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Test
    public void writeFile(){
        File file=new File("/opt/mytestdir");
        for(int i=0;i<1000000;i++){
            try {
                File file1=new File(file.getAbsolutePath()+"/"+i+".text");
                if(file1.exists()){
                    FileOutputStream fs=new FileOutputStream(file1);
                    fs.write("hello the world".getBytes());
                    fs.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Test
    public void readFile(){
        File file=new File("/opt/mytestdir");
        for(int i=0;i<1000000;i++){
            try {
                File file1=new File(file.getAbsolutePath()+"/"+i+".text");
                if(file1.exists()){
                    FileInputStream fs=new FileInputStream(file1);
                    int length=fs.available();
                    byte[] bytes=new byte[length];
                    fs.read(bytes);
                    System.out.println(new String(bytes));
                    fs.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    @Test
    public void delFile(){
        File file=new File("/opt/mytestdir");
        for(int i=0;i<1000000;i++){
            try {
                File file1=new File(file.getAbsolutePath()+"/"+i+".text");
                if(file1.exists()){
                    System.out.print(file1.getPath());
                    file1.delete();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Test
    public void loop(){
        for(int i=0;i<10;i++){
            int b=i;
            new Thread(()->{

               for(int a=0;a<100000;a++){
                   System.out.println(b+"-"+a);
               }
            }).start();
        }
        while (true){

        }
    }
}
