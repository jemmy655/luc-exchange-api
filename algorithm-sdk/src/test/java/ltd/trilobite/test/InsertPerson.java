package ltd.trilobite.test;

import com.alibaba.fastjson.JSON;
import ltd.trilobite.algorithm.miner.dao.MemberStatusDao;
import ltd.trilobite.algorithm.miner.dao.PersonDao;
import ltd.trilobite.algorithm.miner.dao.PersonMinerPowerDao;
import ltd.trilobite.algorithm.miner.dao.entry.Person;
import ltd.trilobite.algorithm.person.PersonService;
import ltd.trilobite.sdk.factory.App;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;
import org.apache.commons.collections.map.HashedMap;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class InsertPerson {
    PersonDao personDao = new PersonDao();
    MemberStatusDao memberStatusDao = new MemberStatusDao();
    PersonService personService = new PersonService();

    @Test
    public void testAddSync() {
        Config.init();
        JdbcTemplet h2master = App.get("h2-master");
        Long start=System.currentTimeMillis();
        for(int i=0;i<1000000;i++){
            h2master.execute("insert into person(person_id,name,user_name,phone_num)values(?,?,?,?)",
            i,"name"+i,"user_name"+i,"phone_num"+i);
        }
        System.out.println(System.currentTimeMillis()-start+"秒");


    }

    @Test
    public void testAddPowerSync() {
        Config.init();
        JdbcTemplet h2master = App.get("h2-master");
        Long start=System.currentTimeMillis();
        for(int i=0;i<1000000;i++){
            h2master.execute("insert into person_miner_power(miner_power_type,run_hour,person_id,create_date)values(?,?,?,?)",
                    1,8,i,new Date());
            h2master.execute("insert into person_miner_power(miner_power_type,run_hour,person_id,create_date)values(?,?,?,?)",
                    2,8,i,new Date());
            h2master.execute("insert into person_miner_power(miner_power_type,run_hour,person_id,create_date)values(?,?,?,?)",
                    3,8,i,new Date());
        }
        System.out.println(System.currentTimeMillis()-start+"秒");
    }

    @Test
    public void testaddMinerSync() {
        Config.init();
        JdbcTemplet h2master = App.get("h2-master");
        Long start=System.currentTimeMillis();
        for(int i=0;i<1000000;i++){
            h2master.execute("insert into person_miner(person_miner_id,miner_id,person_id,use_hour)values(?,?,?,?)",
                    i,1,i,0);
            h2master.execute("insert into person_miner(person_miner_id,miner_id,person_id,use_hour)values(?,?,?,?)",
                    i+1000000,2,i,0);
            h2master.execute("insert into person_miner(person_miner_id,miner_id,person_id,use_hour)values(?,?,?,?)",
                    i+2000000,3,i,0);
        }
        System.out.println(System.currentTimeMillis()-start+"秒");
    }

    @Test
    public void read() {
        Config.init();
        JdbcTemplet h2master = App.get("h2-master");
        Long start=System.currentTimeMillis();
        Connection connection= h2master.connection();
        for(int i=0;i<1000000;i++){

            try {
                PreparedStatement stmt= connection.prepareStatement("select * from person where person_id="+i);
                ResultSet rs=stmt.executeQuery();
                while (rs.next()){
                    System.out.println(rs.getString("person_id"));
                   // rs.getString("person_id");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
         }
       System.out.println(System.currentTimeMillis()-start+"秒");


    }


    List<Map<String, Object>> pool=new Vector<>();

    public void execute(Long start){
        JdbcTemplet h2master = App.get("h2-master");
        new Thread(()->{
            while (true) {
                System.out.println("等待下一步执行");
                System.out.println(System.currentTimeMillis()-start+"毫秒");
                lock();
                while (pool.size() > 0) {


                    Map<String, Object> map = pool.get(0);
                    Integer run_hover = (Integer) map.get("run_hour");
                    Long person_id = (Long) map.get("person_id");
                    List<Map<String, Object>> list = (List<Map<String, Object>>) map.get("children");
                    for (Map<String, Object> row : list) {
                        String sql = "update person_miner set use_hour=" + (((Integer) row.get("use_hour")) + run_hover) + " where person_miner_id=" + row.get("person_miner_id");
                        h2master.execute(sql);
                        System.out.println(sql);
                    }
                    pool.remove(0);
                }
            }
        }).start();


    }

    public void lock(){
        synchronized (InsertPerson.this){
            try {
                InsertPerson.this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void unLock(){
        new Thread(()->{
            synchronized (InsertPerson.this){
                InsertPerson.this.notify();
            }
        }).start();

    }

    //private void addStuct(String person,String run_hour)

    @Test
    public void readPower() {
        Config.init();
        JdbcTemplet h2master = App.get("h2-master");
        Long start=System.currentTimeMillis();
        execute(start);

        Connection connection= h2master.connection();

        for(int i=0;i<1000000;i++){
            Map<String, Object> map=new HashMap<>();

            try {
//                Thread.sleep(1000);
                PreparedStatement stmt= connection.prepareStatement("select sum(run_hour) as run_hour, person_id from person_miner_power where person_id="+i);
                ResultSet rs=stmt.executeQuery();
                while (rs.next()){
                    map.put("run_hour",rs.getInt("run_hour"));
                    map.put("person_id",rs.getLong("person_id"));
                    System.out.println(rs.getString("person_id"));
                    // rs.getString("person_id");
                }

                PreparedStatement stmt1= connection.prepareStatement("select person_miner_id as pid,miner_id,use_hour from person_miner where person_id="+i);
                ResultSet rs1=stmt1.executeQuery();
                List<Map<String,Object>> list=new ArrayList<>();
                while (rs1.next()){
                    Map<String, Object> row=new HashMap<>();
                    row.put("person_miner_id",rs1.getLong("pid"));
                    row.put("miner_id",rs1.getLong("miner_id"));
                    row.put("use_hour",rs1.getInt("use_hour"));
                    list.add(row);
                    // rs.getString("person_id");
                }

                map.put("children",list);
                synchronized (pool){
                    pool.add(map);
                }
                unLock();
            } catch (SQLException e) {
                e.printStackTrace();
            }
//            catch (InterruptedException e) {
//                e.printStackTrace();
//            }
        }
        System.out.println(System.currentTimeMillis()-start+"秒");
        while (true){

        }
    }


}
