package ltd.trilobite.test;

import ltd.trilobite.algorithm.miner.MinerWalletService;
import ltd.trilobite.sdk.config.Conf;
import ltd.trilobite.sdk.config.ConfigFactory;
import ltd.trilobite.sdk.factory.ApplicatonFactory;
import ltd.trilobite.sdk.jdbc.DbSource;
import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;
import org.h2.jdbcx.JdbcDataSource;
import org.junit.Test;

public class TestMinerWallet {
    MinerWalletService minerWalletService=new MinerWalletService();
    void config(){
        ConfigFactory.newInstance().loadLocalConfig();
        JdbcTemplet jdbcTemplet = new JdbcTemplet();
        ApplicatonFactory applicatonFactory = ApplicatonFactory.newInstance();
        jdbcTemplet.setDs(DbSource.get("master"));
        applicatonFactory.add("master", jdbcTemplet);
        JdbcDataSource h2ds=new JdbcDataSource();
        JdbcTemplet jdbcTemplet1 = new JdbcTemplet();
        h2ds.setURL(Conf.get("h2.url"));
        jdbcTemplet1.setDs(h2ds);
        applicatonFactory.add("h2-master", jdbcTemplet1);
    }
    @Test
    public void addForMinerProduct(){
        config();
        minerWalletService.addForMinerProduct(92l,11l);
    }
}
