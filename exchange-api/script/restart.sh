#!/bin/bash
PROJEC_NAME=trilobite-rpc-server
DOCKER_NAME=rpc-server

ssh root@47.104.173.124 << remotessh
cd /home/docker/${PROJEC_NAME}
docker kill ${DOCKER_NAME}
/home/cmd/${PROJEC_NAME}-start.sh
exit
remotessh
