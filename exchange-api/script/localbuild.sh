#!/bin/bash
echo "开始编译打包"

cd ../../
echo "编译程序"
gradle member-api:buildDependent
cd member-api/script
sudo cp ../build/libs/member-api-1.0.0.jar .

cp ./member-api-start.sh /home/cmd

docker kill member-api
docker rmi niuren1/member-api:1.0.0
docker build -t="niuren1/member-api:1.0.0" .
chmod +x /home/cmd/member-api-start.sh
/home/cmd/member-api-start.sh
