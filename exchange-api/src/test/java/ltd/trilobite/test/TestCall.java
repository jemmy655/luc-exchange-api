package ltd.trilobite.test;

import com.alibaba.fastjson.JSON;
import ltd.trilobite.exchange.dao.entry.FoudPerson;
import ltd.trilobite.sdk.jdbc.Id;
import org.junit.Test;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;

public class TestCall {
   // @Test
    public void testTime(){
        FoudPerson foudPerson=new FoudPerson();

        foudPerson.setStartState(1);
        foudPerson.setStartTime(Timestamp.valueOf(LocalDateTime.now()));
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MONTH,6);
        foudPerson.setEndTime(Timestamp.valueOf(LocalDateTime.ofInstant(c.toInstant(),c.getTimeZone().toZoneId())));
        System.out.print(JSON.toJSON(foudPerson));
        System.out.println(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date(foudPerson.getStartTime().getTime())));
        System.out.println(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date(foudPerson.getEndTime().getTime())));
    }
}
