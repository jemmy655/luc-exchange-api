package ltd.trilobite.test;

import com.alibaba.fastjson.JSON;
import ltd.trilobite.exchange.dao.MinerPriceDao;
import ltd.trilobite.exchange.service.PersonMinerService;
import ltd.trilobite.sdk.config.ConfigFactory;
import ltd.trilobite.sdk.factory.ApplicatonFactory;
import ltd.trilobite.sdk.jdbc.DbSource;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;
import org.junit.Test;

public class TestMiner {
    PersonMinerService personMinerService=new PersonMinerService();
    MinerPriceDao minerPriceDao=new MinerPriceDao();
    void config(){
        ConfigFactory.newInstance().loadLocalConfig();
        JdbcTemplet jdbcTemplet = new JdbcTemplet();
        ApplicatonFactory applicatonFactory = ApplicatonFactory.newInstance();
        jdbcTemplet.setDs(DbSource.get("master"));
        applicatonFactory.add("master", jdbcTemplet);
    }
//    @Test
    public void addMine(){
        config();
        personMinerService.addMine(1l,92l);
    }

//    @Test
    public void testQuery(){
        config();
       // JSON.toJSONString()
//        System.out.println(JSON.toJSONString(personMinerService.query("1",92l,"2")));
       // personMinerService.query("1",92l,"1");
    }
//    @Test
    public void testPrice(){
        config();
        //System.out.println( minerPriceDao.findList(1l,92l));
//        minerPriceDao.findList(1l,92l);
    }
}
