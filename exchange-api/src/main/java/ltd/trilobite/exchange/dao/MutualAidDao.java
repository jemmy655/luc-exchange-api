package ltd.trilobite.exchange.dao;

import ltd.trilobite.exchange.dao.entry.MutualAid;
import ltd.trilobite.sdk.factory.App;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;


public class MutualAidDao extends BaseDao<MutualAid>{
    public Result perList1(RestForm form) {
        String Sql="select ma.*,to_char(ma.end_time,'yyyy-MM-dd') as deadline,ma.num-map.num as surplus,p.user_name,p.head_image_url,p.name,t.name_zh as r_name_zh ,t.name_en as r_name_en  from mutual_aid ma\n" +
                "left join (select sum(num) as num,mutual_aid_id from mutual_aid_person group by mutual_aid_id) map on ma.mutual_aid_id=map.mutual_aid_id\n" +
                "left join rewards_type t on ma.rewards_type_id = t.rewards_type_id\n" +
                "left join person p on ma.person_id = p.person_id order by ma.mutual_aid_id desc";
        JdbcTemplet jdbcTemplet = App.get("master");
        return jdbcTemplet.naviList(Sql,form);
    }
}
