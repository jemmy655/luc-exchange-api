package ltd.trilobite.exchange.dao;

import ltd.trilobite.exchange.dao.entry.MaketDetail;
import ltd.trilobite.exchange.dao.entry.MarketInfo;
import ltd.trilobite.sdk.factory.App;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;
import ltd.trilobite.sdk.jdbc.MapRow;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MaketDetailDao extends BaseDao<MaketDetail>{
    static Logger log = LogManager.getLogger(MaketDetailDao.class);
    public Map<String,Object> pool=new HashMap<>();
    public Map<String,Object> cache=new HashMap<>();

    public void addPool(MarketInfo info){
        log.info("接受上传"+info.getSymbol());
        pool.put(info.getSymbol(),info);
    }
    public void clearCache(){
        cache.clear();
    }

    public Result perList(RestForm form) {
        JdbcTemplet jdbcTemplet = App.get("master");
        String key=form.get("marketTypeId")+"-"+form.get("start")+"-"+form.get("pageSize");
//        log.info(key);
//        log.info(pool);
//        if(cache.containsKey(key)){
//            Result result=(Result)cache.get(key);
//
//            List<Map<String, Object>> list= (List<Map<String, Object>>) result.getData();
//            for(Map<String, Object> row:list){
//                row.put("child", pool.get(row.get("symbol")));
//            }
//            return result;
//        }else {
            String sql = "select * from maket_detail where market_type_id=? order by sort_id";
            Result re= jdbcTemplet.naviList(sql, form,Long.parseLong(form.get("marketTypeId")));
//                    new MapRow() {
//                @Override
//                public void execute(Map<String, Object> map) {
//                    map.put("child", pool.get(map.get("symbol")));
//                }
//            }, Long.parseLong(form.get("marketTypeId")));
//            cache.put(key,re);
//            return  re;
////
                    return re;
    }
}
