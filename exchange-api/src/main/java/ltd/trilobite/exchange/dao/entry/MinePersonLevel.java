package ltd.trilobite.exchange.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.Table;
import ltd.trilobite.sdk.jdbc.IdType;

@Table(name = "mine_person_level")
public class MinePersonLevel {
  @Id(type = IdType.Seq)
  private Long minePersonLevelId;
  private Long personId;
  private Integer mineLevelId;


  public Long getMinePersonLevelId() {
    return minePersonLevelId;
  }

  public void setMinePersonLevelId(Long minePersonLevelId) {
    this.minePersonLevelId = minePersonLevelId;
  }


  public Long getPersonId() {
    return personId;
  }

  public void setPersonId(Long personId) {
    this.personId = personId;
  }


  public Integer getMineLevelId() {
    return mineLevelId;
  }

  public void setMineLevelId(Integer mineLevelId) {
    this.mineLevelId = mineLevelId;
  }

}
