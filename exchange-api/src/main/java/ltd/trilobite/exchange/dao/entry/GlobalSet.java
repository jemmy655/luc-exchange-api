package ltd.trilobite.exchange.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.Table;
import ltd.trilobite.sdk.jdbc.IdType;

@Table(name = "global_set")
public class GlobalSet {
  @Id
  private Integer globalSetId;
  private String constName;
  private String constVal;


  public Integer getGlobalSetId() {
    return globalSetId;
  }

  public void setGlobalSetId(Integer globalSetId) {
    this.globalSetId = globalSetId;
  }


  public String getConstName() {
    return constName;
  }

  public void setConstName(String constName) {
    this.constName = constName;
  }


  public String getConstVal() {
    return constVal;
  }

  public void setConstVal(String constVal) {
    this.constVal = constVal;
  }

}
