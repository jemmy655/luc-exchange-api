package ltd.trilobite.exchange.dao;


import ltd.trilobite.exchange.dao.entry.ToLoan;
import ltd.trilobite.sdk.factory.App;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;

public class ToLoanDao extends BaseDao<ToLoan>{
    public Result perList(RestForm form) {
        String Sql="select tl.*,p.name,p.user_name,p.head_image_url,r.rewards_type_id,r.name_en as r_name_en,r.name_zh r_name_zh from to_loan tl left join person p on p.person_id=tl.person_id\n" +
                "left join rewards_type r on r.rewards_type_id=tl.rewards_type_id where tl.is_to_loan="+form.get("isToLoan")+" order by to_loan_id desc";
        JdbcTemplet jdbcTemplet = App.get("master");
        return jdbcTemplet.naviList(Sql,form);
    }
}
