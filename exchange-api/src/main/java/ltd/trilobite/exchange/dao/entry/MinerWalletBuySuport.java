package ltd.trilobite.exchange.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.Table;
import ltd.trilobite.sdk.jdbc.IdType;

@Table(name = "miner_wallet_buy_suport")
public class MinerWalletBuySuport {
  @Id(type = IdType.Seq)
  private Long minerWalletBuySuportId;
  private Long minerId;
  private Integer suport;


  public Long getMinerWalletBuySuportId() {
    return minerWalletBuySuportId;
  }

  public void setMinerWalletBuySuportId(Long minerWalletBuySuportId) {
    this.minerWalletBuySuportId = minerWalletBuySuportId;
  }


  public Long getMinerId() {
    return minerId;
  }

  public void setMinerId(Long minerId) {
    this.minerId = minerId;
  }


  public Integer getSuport() {
    return suport;
  }

  public void setSuport(Integer suport) {
    this.suport = suport;
  }

}
