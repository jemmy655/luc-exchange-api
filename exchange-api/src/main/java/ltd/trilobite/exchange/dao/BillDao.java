package ltd.trilobite.exchange.dao;

import ltd.trilobite.exchange.dao.entry.Bill;
import ltd.trilobite.sdk.factory.App;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;


public class BillDao extends BaseDao<Bill>{
    public Double balance(Long personId,Integer typeId){
        JdbcTemplet jdbcTemplet = App.get("master");
        String sql = "select case when sum(num) is null then 0 else sum(num) end as balance from bill where person_id=? and rewards_type_id=?";
        return jdbcTemplet.getObject(sql, java.math.BigDecimal.class,personId, typeId).doubleValue();
    }
}
