package ltd.trilobite.exchange.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.Table;
import ltd.trilobite.sdk.jdbc.IdType;

import java.sql.Timestamp;
import java.util.Date;

@Table(name = "currency_market")
public class CurrencyMarket {
  @Id(type = IdType.Seq)
  private Long currencyMarketId;
  private Integer action;
  private Double price;
  private Long personId;
  private Double num;
  private java.sql.Timestamp createTime;
  private Double numMin;
  private Double numMax;
  private Integer onOff;
  private String token;
  private Integer useCurrency;
  private Integer rewardsTypeId;
  private Date updateDate;

  public Long getCurrencyMarketId() {
    return currencyMarketId;
  }

  public void setCurrencyMarketId(Long currencyMarketId) {
    this.currencyMarketId = currencyMarketId;
  }

  public Integer getAction() {
    return action;
  }

  public void setAction(Integer action) {
    this.action = action;
  }

  public Double getPrice() {
    return price;
  }

  public void setPrice(Double price) {
    this.price = price;
  }

  public Long getPersonId() {
    return personId;
  }

  public void setPersonId(Long personId) {
    this.personId = personId;
  }

  public Double getNum() {
    return num;
  }

  public void setNum(Double num) {
    this.num = num;
  }

  public Timestamp getCreateTime() {
    return createTime;
  }

  public void setCreateTime(Timestamp createTime) {
    this.createTime = createTime;
  }

  public Double getNumMin() {
    return numMin;
  }

  public void setNumMin(Double numMin) {
    this.numMin = numMin;
  }

  public Double getNumMax() {
    return numMax;
  }

  public void setNumMax(Double numMax) {
    this.numMax = numMax;
  }

  public Integer getOnOff() {
    return onOff;
  }

  public void setOnOff(Integer onOff) {
    this.onOff = onOff;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public Integer getUseCurrency() {
    return useCurrency;
  }

  public void setUseCurrency(Integer useCurrency) {
    this.useCurrency = useCurrency;
  }

  public Integer getRewardsTypeId() {
    return rewardsTypeId;
  }

  public void setRewardsTypeId(Integer rewardsTypeId) {
    this.rewardsTypeId = rewardsTypeId;
  }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}
