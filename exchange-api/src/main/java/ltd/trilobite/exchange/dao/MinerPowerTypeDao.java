package ltd.trilobite.exchange.dao;

import ltd.trilobite.exchange.dao.entry.MinerPowerType;
import ltd.trilobite.exchange.dao.entry.MinerProduce;
import ltd.trilobite.sdk.factory.App;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;
import ltd.trilobite.sdk.status.Result;

import java.util.List;
import java.util.Map;

public class MinerPowerTypeDao extends BaseDao<MinerPowerType>{
    public List<Map<String,Object>> findList(MinerPowerType minerPowerType){
        JdbcTemplet jdbcTemplet = App.get("master");
        String sql="select miner_power_type.*,t.name_en as r_name_en,t.name_zh r_name_zh   from miner_power_type left join rewards_type t on miner_power_type.rewards_type_id = t.rewards_type_id order by miner_power_type.miner_power_type asc";
        return jdbcTemplet.findList(sql);
    }
}
