package ltd.trilobite.exchange.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.Table;

@Table(name = "rewards_type_price")
public class RewardsTypePrice {
  @Id
  private Integer rewardsTypeId;
  private Double anyPrice;
  private Double usPrice;


  public Integer getRewardsTypeId() {
    return rewardsTypeId;
  }

  public void setRewardsTypeId(Integer rewardsTypeId) {
    this.rewardsTypeId = rewardsTypeId;
  }


  public Double getAnyPrice() {
    return anyPrice;
  }

  public void setAnyPrice(Double anyPrice) {
    this.anyPrice = anyPrice;
  }


  public Double getUsPrice() {
    return usPrice;
  }

  public void setUsPrice(Double usPrice) {
    this.usPrice = usPrice;
  }

}
