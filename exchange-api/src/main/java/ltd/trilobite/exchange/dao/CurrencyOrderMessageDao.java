package ltd.trilobite.exchange.dao;

import ltd.trilobite.exchange.dao.entry.CurrencyOrderMessage;
import ltd.trilobite.sdk.factory.App;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;

public class CurrencyOrderMessageDao extends BaseDao<CurrencyOrderMessage>{
    public Result perlist(RestForm form) {
        JdbcTemplet jdbcTemplet = App.get("master");
        StringBuffer mainsql=new StringBuffer();
        mainsql.append("select * from currency_order_message where currency_order="+form.get("currencyOrder"));
        StringBuilder sql=new StringBuilder();
        sql.append("select p.name,p.user_name,p.head_image_url,com.* from("+mainsql+") com " +
                "left join person p on p.person_id=com.person_id order by com.create_time");

        sql.append(" LIMIT "+form.get("pageSize")+" offset "+form.get("start"));
        StringBuilder countsql=new StringBuilder();
        countsql.append("select count(1) from ("+mainsql+")a");
        return jdbcTemplet.naviList(sql.toString(),countsql.toString(),null);
    }
}
