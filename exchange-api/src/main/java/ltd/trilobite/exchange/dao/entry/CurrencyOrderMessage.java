package ltd.trilobite.exchange.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.Table;
import ltd.trilobite.sdk.jdbc.IdType;

@Table(name = "currency_order_message")
public class CurrencyOrderMessage {
  @Id(type = IdType.Seq)
  private Long currencyOrderMessageId;
  private String message;
  private Integer cType;
  private Long currencyOrder;
  private Long personId;
  private java.sql.Timestamp createTime;


  public Long getCurrencyOrderMessageId() {
    return currencyOrderMessageId;
  }

  public void setCurrencyOrderMessageId(Long currencyOrderMessageId) {
    this.currencyOrderMessageId = currencyOrderMessageId;
  }


  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }


  public Integer getCType() {
    return cType;
  }

  public void setCType(Integer cType) {
    this.cType = cType;
  }


  public Long getCurrencyOrder() {
    return currencyOrder;
  }

  public void setCurrencyOrder(Long currencyOrder) {
    this.currencyOrder = currencyOrder;
  }


  public Long getPersonId() {
    return personId;
  }

  public void setPersonId(Long personId) {
    this.personId = personId;
  }


  public java.sql.Timestamp getCreateTime() {
    return createTime;
  }

  public void setCreateTime(java.sql.Timestamp createTime) {
    this.createTime = createTime;
  }

}
