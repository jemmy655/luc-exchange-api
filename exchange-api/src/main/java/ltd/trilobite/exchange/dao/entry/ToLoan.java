package ltd.trilobite.exchange.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.Table;
import ltd.trilobite.sdk.jdbc.IdType;

@Table(name = "to_loan")
public class ToLoan {
 @Id(type = IdType.Seq)
  private Long toLoanId;
  private Integer isToLoan;
  private Integer num;
  private Integer valTime;
  private Integer rewardsTypeId;
  private Long personId;


  public Long getToLoanId() {
    return toLoanId;
  }

  public void setToLoanId(Long toLoanId) {
    this.toLoanId = toLoanId;
  }


  public Integer getIsToLoan() {
    return isToLoan;
  }

  public void setIsToLoan(Integer isToLoan) {
    this.isToLoan = isToLoan;
  }


  public Integer getNum() {
    return num;
  }

  public void setNum(Integer num) {
    this.num = num;
  }


  public Integer getValTime() {
    return valTime;
  }

  public void setValTime(Integer valTime) {
    this.valTime = valTime;
  }


  public Integer getRewardsTypeId() {
    return rewardsTypeId;
  }

  public void setRewardsTypeId(Integer rewardsTypeId) {
    this.rewardsTypeId = rewardsTypeId;
  }


  public Long getPersonId() {
    return personId;
  }

  public void setPersonId(Long personId) {
    this.personId = personId;
  }

}
