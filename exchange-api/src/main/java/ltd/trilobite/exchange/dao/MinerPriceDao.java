package ltd.trilobite.exchange.dao;

import ltd.trilobite.exchange.dao.entry.MinerPrice;
import ltd.trilobite.sdk.factory.App;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;

import java.util.List;
import java.util.Map;

public class MinerPriceDao extends BaseDao<MinerPrice>{
    public List<Map<String,Object>> findBillBalanceList(Long minerId, Long PersonId){
        JdbcTemplet jdbcTemplet = App.get("master");
        String sql="select pp.*,r.name_en as r_name_en,r.name_zh as r_name_zh,case when b.balance is null then 0 else b.balance end as balance from" +
                "(select * from miner_price where miner_id=?) pp" +
                " left join rewards_type r on r.rewards_type_id=pp.rewards_type_id" +
                " left join (" +
                "    select" +
                "           sum(num)  as balance" +
                "        ,rewards_type_id from bill where person_id=? group by rewards_type_id" +
                "    )b on r.rewards_type_id = b.rewards_type_id";
        return jdbcTemplet.findList(sql,minerId,PersonId);
    }

    public List<Map<String,Object>> findList(MinerPrice price){
        JdbcTemplet jdbcTemplet = App.get("master");
        String sql="select mp.miner_price_id,mp.price,rt.name_zh,rt.name_en,mp.miner_id,mp.rewards_type_id from (select * from miner_price  where miner_id=?) mp left join rewards_type rt on rt.rewards_type_id=mp.rewards_type_id order by miner_price_id asc";
        return jdbcTemplet.findList(sql,price.getMinerId());
    }


    public List<Map<String,Object>> findRewardsBillBalanceList(Long minerId, Long PersonId){
        JdbcTemplet jdbcTemplet = App.get("master");
        String sql="select pp.*,r.name_en as r_name_en,r.name_zh as r_name_zh,case when b.balance is null then 0 else b.balance end as balance from" +
                "(select * from miner_price where miner_id=?) pp" +
                " left join rewards_type r on r.rewards_type_id=pp.rewards_type_id" +
                " left join (" +
                "    select" +
                "           sum(num)  as balance" +
                "        ,rewards_type_id from rewards_bill where person_id=? group by rewards_type_id" +
                "    )b on r.rewards_type_id = b.rewards_type_id";
        return jdbcTemplet.findList(sql,minerId,PersonId);
    }


    public List<Map<String,Object>> findMinerWalletBalanceList(Long minerId, Long PersonId,Integer suport){
        JdbcTemplet jdbcTemplet = App.get("master");
        String sql="select pp.*,r.name_en as r_name_en,r.name_zh as r_name_zh,coalesce(b.balance,0) balance from(select * from miner_price where miner_id=?) pp left join rewards_type r on r.rewards_type_id=pp.rewards_type_id left join (    select           sum(num)  as balance        ,rewards_type_id from miner_wallet where person_id=? and wallet_suport=? group by rewards_type_id    )b on r.rewards_type_id = b.rewards_type_id";
        return jdbcTemplet.findList(sql,minerId,PersonId,suport);
    }


}
