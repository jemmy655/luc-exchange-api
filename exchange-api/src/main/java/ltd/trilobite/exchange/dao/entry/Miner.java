package ltd.trilobite.exchange.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.Table;
import ltd.trilobite.sdk.jdbc.IdType;

@Table(name = "miner")
public class Miner {
  @Id(type = IdType.Seq)
  private Long minerId;
  private String nameZh;
  private String nameEn;

  private String imageUrl;
  private Integer runHour;
  private Integer soreNum;
  private Integer buyGoup;
  private Integer onOff;
  private Long consensus;
  private Integer buyRect;
  private Integer mineTypeId;
  private Integer algorithm;


  public Long getMinerId() {
    return minerId;
  }

  public void setMinerId(Long minerId) {
    this.minerId = minerId;
  }


  public String getNameZh() {
    return nameZh;
  }

  public void setNameZh(String nameZh) {
    this.nameZh = nameZh;
  }


  public String getNameEn() {
    return nameEn;
  }

  public void setNameEn(String nameEn) {
    this.nameEn = nameEn;
  }



  public void setImageUrl(String imageUrl) {
    this.imageUrl = imageUrl;
  }


  public Integer getRunHour() {
    return runHour;
  }

  public void setRunHour(Integer runHour) {
    this.runHour = runHour;
  }


  public Integer getSoreNum() {
    return soreNum;
  }

  public void setSoreNum(Integer soreNum) {
    this.soreNum = soreNum;
  }


  public Integer getBuyGoup() {
    return buyGoup;
  }

  public void setBuyGoup(Integer buyGoup) {
    this.buyGoup = buyGoup;
  }


  public Integer getOnOff() {
    return onOff;
  }

  public void setOnOff(Integer onOff) {
    this.onOff = onOff;
  }


  public Long getConsensus() {
    return consensus;
  }

  public void setConsensus(Long consensus) {
    this.consensus = consensus;
  }


  public Integer getBuyRect() {
    return buyRect;
  }

  public void setBuyRect(Integer buyRect) {
    this.buyRect = buyRect;
  }


  public Integer getMineTypeId() {
    return mineTypeId;
  }

  public void setMineTypeId(Integer mineTypeId) {
    this.mineTypeId = mineTypeId;
  }


  public Integer getAlgorithm() {
    return algorithm;
  }

  public void setAlgorithm(Integer algorithm) {
    this.algorithm = algorithm;
  }

}
