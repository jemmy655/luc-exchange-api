package ltd.trilobite.exchange.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.Table;
import ltd.trilobite.sdk.jdbc.IdType;

@Table(name = "product_miner")
public class ProductMiner {
  @Id(type = IdType.Seq)
  private Long productMinerId;
  private Long productId;
  private Long minerId;


  public Long getProductMinerId() {
    return productMinerId;
  }

  public void setProductMinerId(Long productMinerId) {
    this.productMinerId = productMinerId;
  }


  public Long getProductId() {
    return productId;
  }

  public void setProductId(Long productId) {
    this.productId = productId;
  }


  public Long getMinerId() {
    return minerId;
  }

  public void setMinerId(Long minerId) {
    this.minerId = minerId;
  }

}
