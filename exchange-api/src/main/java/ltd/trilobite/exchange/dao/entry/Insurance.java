package ltd.trilobite.exchange.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.Table;
import ltd.trilobite.sdk.jdbc.IdType;

@Table(name = "insurance")
public class Insurance {
  @Id(type = IdType.Seq)
  private Long insuranceId;
  private String nameZh;
  private String nameEn;
  private String descriptionZh;
  private String descriptionEn;
  private Integer valTime;
  private Integer rewardsTypeId;
  private Double price;
  private Long shopId;

  public Long getInsuranceId() {
    return insuranceId;
  }

  public void setInsuranceId(Long insuranceId) {
    this.insuranceId = insuranceId;
  }


  public String getNameZh() {
    return nameZh;
  }

  public void setNameZh(String nameZh) {
    this.nameZh = nameZh;
  }


  public String getNameEn() {
    return nameEn;
  }

  public void setNameEn(String nameEn) {
    this.nameEn = nameEn;
  }


  public String getDescriptionZh() {
    return descriptionZh;
  }

  public void setDescriptionZh(String descriptionZh) {
    this.descriptionZh = descriptionZh;
  }


  public String getDescriptionEn() {
    return descriptionEn;
  }

  public void setDescriptionEn(String descriptionEn) {
    this.descriptionEn = descriptionEn;
  }


  public Integer getValTime() {
    return valTime;
  }

  public void setValTime(Integer valTime) {
    this.valTime = valTime;
  }


  public Integer getRewardsTypeId() {
    return rewardsTypeId;
  }

  public void setRewardsTypeId(Integer rewardsTypeId) {
    this.rewardsTypeId = rewardsTypeId;
  }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }
}
