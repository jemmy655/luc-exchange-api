package ltd.trilobite.exchange.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.Table;
import ltd.trilobite.sdk.jdbc.IdType;

@Table(name = "insurance_person")
public class InsurancePerson {
  @Id(type = IdType.Seq)
  private Long insurancePersonId;
  private java.sql.Timestamp startTime;
  private java.sql.Timestamp endTime;
  private Integer startState;
  private Long personId;
  private Long insuranceId;


  public Long getInsurancePersonId() {
    return insurancePersonId;
  }

  public void setInsurancePersonId(Long insurancePersonId) {
    this.insurancePersonId = insurancePersonId;
  }


  public java.sql.Timestamp getStartTime() {
    return startTime;
  }

  public void setStartTime(java.sql.Timestamp startTime) {
    this.startTime = startTime;
  }


  public java.sql.Timestamp getEndTime() {
    return endTime;
  }

  public void setEndTime(java.sql.Timestamp endTime) {
    this.endTime = endTime;
  }


  public Integer getStartState() {
    return startState;
  }

  public void setStartState(Integer startState) {
    this.startState = startState;
  }


  public Long getPersonId() {
    return personId;
  }

  public void setPersonId(Long personId) {
    this.personId = personId;
  }


  public Long getInsuranceId() {
    return insuranceId;
  }

  public void setInsuranceId(Long insuranceId) {
    this.insuranceId = insuranceId;
  }

}
