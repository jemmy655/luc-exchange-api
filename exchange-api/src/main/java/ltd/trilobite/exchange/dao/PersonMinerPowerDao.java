package ltd.trilobite.exchange.dao;


import ltd.trilobite.exchange.dao.entry.PersonMinerPower;
import ltd.trilobite.sdk.factory.App;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public class PersonMinerPowerDao extends BaseDao<PersonMinerPower>{
    public List<Map<String, Object>> dayPower(Long personId) {
        JdbcTemplet jdbcTemplet = App.get("master");
        String sql="select coalesce(pmp.run_hour,0) as run_hour,mpt.* from (select * from person_miner_power where person_id=? and create_date=current_date) pmp right join miner_power_type mpt on mpt.miner_power_type=pmp.miner_power_type";
        return jdbcTemplet.findList(sql,personId);
    }

    public Integer prvDayPower(Long personId) {
        JdbcTemplet jdbcTemplet = App.get("master");
        String sql="select coalesce(sum(run_hour),0) from person_miner_power where person_id=? and create_date=current_date-1";
        return jdbcTemplet.getObject(sql, BigDecimal.class,personId).intValue();
    }
}
