package ltd.trilobite.exchange.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.Table;
import ltd.trilobite.sdk.jdbc.IdType;

@Table(name = "mutual_aid")
public class MutualAid {
  @Id(type = IdType.Seq)
  private Long mutualAidId;
  private Long num;
  private String titleZh;
  private String titleEn;
  private java.sql.Timestamp endTime;
  private Integer startState;
  private String description;
  private Integer process;
  private Long personId;
  private Integer rewardsTypeId;
  private String descriptionEn;


  public Long getMutualAidId() {
    return mutualAidId;
  }

  public void setMutualAidId(Long mutualAidId) {
    this.mutualAidId = mutualAidId;
  }


  public Long getNum() {
    return num;
  }

  public void setNum(Long num) {
    this.num = num;
  }


  public String getTitleZh() {
    return titleZh;
  }

  public void setTitleZh(String titleZh) {
    this.titleZh = titleZh;
  }


  public String getTitleEn() {
    return titleEn;
  }

  public void setTitleEn(String titleEn) {
    this.titleEn = titleEn;
  }


  public java.sql.Timestamp getEndTime() {
    return endTime;
  }

  public void setEndTime(java.sql.Timestamp endTime) {
    this.endTime = endTime;
  }


  public Integer getStartState() {
    return startState;
  }

  public void setStartState(Integer startState) {
    this.startState = startState;
  }


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }


  public Integer getProcess() {
    return process;
  }

  public void setProcess(Integer process) {
    this.process = process;
  }


  public Long getPersonId() {
    return personId;
  }

  public void setPersonId(Long personId) {
    this.personId = personId;
  }


  public Integer getRewardsTypeId() {
    return rewardsTypeId;
  }

  public void setRewardsTypeId(Integer rewardsTypeId) {
    this.rewardsTypeId = rewardsTypeId;
  }


  public String getDescriptionEn() {
    return descriptionEn;
  }

  public void setDescriptionEn(String descriptionEn) {
    this.descriptionEn = descriptionEn;
  }

}
