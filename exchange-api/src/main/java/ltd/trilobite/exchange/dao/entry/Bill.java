package ltd.trilobite.exchange.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.Table;
import ltd.trilobite.sdk.jdbc.IdType;

@Table(name = "bill")
public class Bill {
  @Id(type = IdType.Seq)
  private Long billId;
  private Long personId;
  private java.sql.Timestamp createTime;
  private String description;
  private Integer state;
  private Double num;
  private Long srcId;
  private Integer rewardsTypeId;
  private Integer rewardsActionId;


  public Long getBillId() {
    return billId;
  }

  public void setBillId(Long billId) {
    this.billId = billId;
  }


  public Long getPersonId() {
    return personId;
  }

  public void setPersonId(Long personId) {
    this.personId = personId;
  }



  public java.sql.Timestamp getCreateTime() {
    return createTime;
  }

  public void setCreateTime(java.sql.Timestamp createTime) {
    this.createTime = createTime;
  }


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }


  public Integer getState() {
    return state;
  }

  public void setState(Integer state) {
    this.state = state;
  }


  public Double getNum() {
    return num;
  }

  public void setNum(Double num) {
    this.num = num;
  }


  public Long getSrcId() {
    return srcId;
  }

  public void setSrcId(Long srcId) {
    this.srcId = srcId;
  }


  public Integer getRewardsTypeId() {
    return rewardsTypeId;
  }

  public void setRewardsTypeId(Integer rewardsTypeId) {
    this.rewardsTypeId = rewardsTypeId;
  }


  public Integer getRewardsActionId() {
    return rewardsActionId;
  }

  public void setRewardsActionId(Integer rewardsActionId) {
    this.rewardsActionId = rewardsActionId;
  }

}
