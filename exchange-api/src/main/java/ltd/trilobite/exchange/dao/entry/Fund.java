package ltd.trilobite.exchange.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.Table;
import ltd.trilobite.sdk.jdbc.IdType;

@Table(name = "fund")
public class Fund {
  @Id(type = IdType.Seq)

  private Long fundId;
  private String nameZh;
  private java.sql.Timestamp createTime;
  private String description;
  private Integer countDay;
  private Double dayPrice;
  private Integer rewardsTypeId;
  private String nameEn;
  private String descriptionEn;
  private Double lowNum;
  private Double maxNum;
  private Long fType;
  private Integer pRewardsTypeId;
  private Integer onOff;


  public Long getFundId() {
    return fundId;
  }

  public void setFundId(Long fundId) {
    this.fundId = fundId;
  }


  public String getNameZh() {
    return nameZh;
  }

  public void setNameZh(String nameZh) {
    this.nameZh = nameZh;
  }


  public java.sql.Timestamp getCreateTime() {
    return createTime;
  }

  public void setCreateTime(java.sql.Timestamp createTime) {
    this.createTime = createTime;
  }


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }


  public Integer getCountDay() {
    return countDay;
  }

  public void setCountDay(Integer countDay) {
    this.countDay = countDay;
  }


  public Double getDayPrice() {
    return dayPrice;
  }

  public void setDayPrice(Double dayPrice) {
    this.dayPrice = dayPrice;
  }


  public Integer getRewardsTypeId() {
    return rewardsTypeId;
  }

  public void setRewardsTypeId(Integer rewardsTypeId) {
    this.rewardsTypeId = rewardsTypeId;
  }


  public String getNameEn() {
    return nameEn;
  }

  public void setNameEn(String nameEn) {
    this.nameEn = nameEn;
  }


  public String getDescriptionEn() {
    return descriptionEn;
  }

  public void setDescriptionEn(String descriptionEn) {
    this.descriptionEn = descriptionEn;
  }


  public Double getLowNum() {
    return lowNum;
  }

  public void setLowNum(Double lowNum) {
    this.lowNum = lowNum;
  }


  public Double getMaxNum() {
    return maxNum;
  }

  public void setMaxNum(Double maxNum) {
    this.maxNum = maxNum;
  }


  public Long getFType() {
    return fType;
  }

  public void setFType(Long fType) {
    this.fType = fType;
  }


  public Integer getPRewardsTypeId() {
    return pRewardsTypeId;
  }

  public void setPRewardsTypeId(Integer pRewardsTypeId) {
    this.pRewardsTypeId = pRewardsTypeId;
  }

  public Long getfType() {
    return fType;
  }

  public void setfType(Long fType) {
    this.fType = fType;
  }

  public Integer getpRewardsTypeId() {
    return pRewardsTypeId;
  }

  public void setpRewardsTypeId(Integer pRewardsTypeId) {
    this.pRewardsTypeId = pRewardsTypeId;
  }

  public Integer getOnOff() {
    return onOff;
  }

  public void setOnOff(Integer onOff) {
    this.onOff = onOff;
  }
}
