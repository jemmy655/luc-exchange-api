package ltd.trilobite.exchange.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.Table;
import ltd.trilobite.sdk.jdbc.IdType;

@Table(name = "currency_order")
public class CurrencyOrder {
  @Id
  private Long currencyOrder;
  private String price;
  private Integer state;
  private Double num;
  private String token;
  private java.sql.Timestamp createTime;
  private Long buyId;
  private Long sellId;
  private Long currencyMarketId;
  private Integer evaluateNum;
  private String evaluateMessage;


  public Long getCurrencyOrder() {
    return currencyOrder;
  }

  public void setCurrencyOrder(Long currencyOrder) {
    this.currencyOrder = currencyOrder;
  }


  public String getPrice() {
    return price;
  }

  public void setPrice(String price) {
    this.price = price;
  }


  public Integer getState() {
    return state;
  }

  public void setState(Integer state) {
    this.state = state;
  }


  public Double getNum() {
    return num;
  }

  public void setNum(Double num) {
    this.num = num;
  }


  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }


  public java.sql.Timestamp getCreateTime() {
    return createTime;
  }

  public void setCreateTime(java.sql.Timestamp createTime) {
    this.createTime = createTime;
  }


  public Long getBuyId() {
    return buyId;
  }

  public void setBuyId(Long buyId) {
    this.buyId = buyId;
  }


  public Long getSellId() {
    return sellId;
  }

  public void setSellId(Long sellId) {
    this.sellId = sellId;
  }


  public Long getCurrencyMarketId() {
    return currencyMarketId;
  }

  public void setCurrencyMarketId(Long currencyMarketId) {
    this.currencyMarketId = currencyMarketId;
  }


  public Integer getEvaluateNum() {
    return evaluateNum;
  }

  public void setEvaluateNum(Integer evaluateNum) {
    this.evaluateNum = evaluateNum;
  }


  public String getEvaluateMessage() {
    return evaluateMessage;
  }

  public void setEvaluateMessage(String evaluateMessage) {
    this.evaluateMessage = evaluateMessage;
  }

}
