package ltd.trilobite.exchange.dao;

import ltd.trilobite.exchange.dao.entry.CurrencyOrder;
import ltd.trilobite.sdk.factory.App;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;
import ltd.trilobite.sdk.util.Util;


public class CurrencyOrderDao extends BaseDao<CurrencyOrder>{

    /**
     * 当前市场信息下是否有订单
     * @param currencyMarketId
     * @return
     */
    public boolean hasOrder(Long currencyMarketId){
        String sql="select count(1) as count from currency_order where currency_market_id=?";
        JdbcTemplet jdbcTemplet = App.get("master");
        return jdbcTemplet.getObject(sql,Long.class,currencyMarketId)>0;
    }

    public Result perlist(RestForm form,String personId) {
        JdbcTemplet jdbcTemplet = App.get("master");
        StringBuffer mainsql=new StringBuffer();
        mainsql.append("select * from currency_order where buy_id="+personId+" or sell_id="+personId+"");
        if(Util.isNotEmpty(form.get("state"))){
            mainsql.append("  and state="+form.get("state"));
        }
        StringBuilder sql=new StringBuilder();
        sql.append("select" +
                "           corder.*,cm.price,t.name_en as t_name_zh,t.name_zh as t_name_zh," +
                "           p1.user_name as buy_user_name,p.user_name as sell_user_name" +
                "    from" +
                "    (" +mainsql+
                "    ) corder left join currency_market cm on corder.currency_market_id=cm.currency_market_id" +
                "    left join rewards_type t on cm.rewards_type_id = t.rewards_type_id" +
                "    left join person p on corder.buy_id = p.person_id" +
                "    left join person p1 on corder.sell_id = p1.person_id" +
                "    order by corder.create_time desc ");

        sql.append(" LIMIT "+form.get("pageSize")+" offset "+form.get("start"));
        StringBuilder countsql=new StringBuilder();
        countsql.append("select count(1) from ("+mainsql+")a");
        return jdbcTemplet.naviList(sql.toString(),countsql.toString(),null);
    }
}
