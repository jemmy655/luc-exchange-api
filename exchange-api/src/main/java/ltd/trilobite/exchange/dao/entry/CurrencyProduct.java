package ltd.trilobite.exchange.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.Table;
import ltd.trilobite.sdk.jdbc.IdType;

@Table(name = "currency_product")
public class CurrencyProduct {
  @Id(type = IdType.Seq)
  private Long currencyProductId;
  private Long currencyTypeId;
  private String tradingVol;
  private String dayQuo;
  private String cnyPrice;
  private String currPrice;
  private String util;
  private String zhName;
  private String ico;
  private String enName;
  private String dayHPrice;
  private String dayLPrice;
  private String dayVol;


  public Long getCurrencyProductId() {
    return currencyProductId;
  }

  public void setCurrencyProductId(Long currencyProductId) {
    this.currencyProductId = currencyProductId;
  }


  public Long getCurrencyTypeId() {
    return currencyTypeId;
  }

  public void setCurrencyTypeId(Long currencyTypeId) {
    this.currencyTypeId = currencyTypeId;
  }


  public String getTradingVol() {
    return tradingVol;
  }

  public void setTradingVol(String tradingVol) {
    this.tradingVol = tradingVol;
  }


  public String getDayQuo() {
    return dayQuo;
  }

  public void setDayQuo(String dayQuo) {
    this.dayQuo = dayQuo;
  }


  public String getCnyPrice() {
    return cnyPrice;
  }

  public void setCnyPrice(String cnyPrice) {
    this.cnyPrice = cnyPrice;
  }


  public String getCurrPrice() {
    return currPrice;
  }

  public void setCurrPrice(String currPrice) {
    this.currPrice = currPrice;
  }


  public String getUtil() {
    return util;
  }

  public void setUtil(String util) {
    this.util = util;
  }


  public String getZhName() {
    return zhName;
  }

  public void setZhName(String zhName) {
    this.zhName = zhName;
  }


  public String getIco() {
    return ico;
  }

  public void setIco(String ico) {
    this.ico = ico;
  }


  public String getEnName() {
    return enName;
  }

  public void setEnName(String enName) {
    this.enName = enName;
  }


  public String getDayHPrice() {
    return dayHPrice;
  }

  public void setDayHPrice(String dayHPrice) {
    this.dayHPrice = dayHPrice;
  }


  public String getDayLPrice() {
    return dayLPrice;
  }

  public void setDayLPrice(String dayLPrice) {
    this.dayLPrice = dayLPrice;
  }


  public String getDayVol() {
    return dayVol;
  }

  public void setDayVol(String dayVol) {
    this.dayVol = dayVol;
  }

}
