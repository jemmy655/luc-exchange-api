package ltd.trilobite.exchange.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.IdType;
import ltd.trilobite.sdk.jdbc.Table;


@Table(name = "miner_price")
public class MinerPrice {
  @Id(type = IdType.Seq)
  private Long minerPriceId;
  private Long minerId;
  private Integer rewardsTypeId;
  private Double price;


  public Long getMinerPriceId() {
    return minerPriceId;
  }

  public void setMinerPriceId(Long minerPriceId) {
    this.minerPriceId = minerPriceId;
  }


  public Long getMinerId() {
    return minerId;
  }

  public void setMinerId(Long minerId) {
    this.minerId = minerId;
  }


  public Integer getRewardsTypeId() {
    return rewardsTypeId;
  }

  public void setRewardsTypeId(Integer rewardsTypeId) {
    this.rewardsTypeId = rewardsTypeId;
  }


  public Double getPrice() {
    return price;
  }

  public void setPrice(Double price) {
    this.price = price;
  }

}
