package ltd.trilobite.exchange.dao;

import ltd.trilobite.exchange.dao.entry.GlobalSet;


public class GlobalSetDao extends BaseDao<GlobalSet>{
    public Integer getInt(Integer id){
        GlobalSet param=new GlobalSet();
        return Integer.parseInt(this.findOne(param,GlobalSet.class).getConstVal());
    }
}
