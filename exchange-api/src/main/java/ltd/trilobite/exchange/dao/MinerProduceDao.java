package ltd.trilobite.exchange.dao;


import ltd.trilobite.exchange.dao.entry.MinerProduce;
import ltd.trilobite.sdk.factory.App;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;

import java.util.List;
import java.util.Map;


public class MinerProduceDao extends BaseDao<MinerProduce>{
    public List<Map<String,Object>> findList(MinerProduce minerProduce){
        JdbcTemplet jdbcTemplet = App.get("master");
        String sql="select mp.miner_produce_id,mp.miner_id,rt.name_en,rt.name_zh,rt.rewards_type_id,mp.hour_val,mp.cout_val,mp.wallet_suport,mws.name_zh as mws_name_zh,mws.name_en as mws_name_en from(select * from miner_produce where miner_id=?) mp" +
                " left join rewards_type rt on rt.rewards_type_id=mp.rewards_type_id" +
                " left join miner_wallet_suport mws on mws.miner_wallet_suport_id=mp.wallet_suport order by mp.miner_produce_id asc";
        return jdbcTemplet.findList(sql,minerProduce.getMinerId());
    }

    /**
     * 是否矿机产出的货币
     * @rewardTypeId 货币编号
     * @return
     */
    public boolean isSuport(Integer rewardTypeId){
        return false;
    }

}
