package ltd.trilobite.exchange.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.Table;
import ltd.trilobite.sdk.jdbc.IdType;

@Table(name = "mine_level")
public class MineLevel {
  @Id
  private Integer mineLevelId;
  private String nameZh;
  private String nameEn;


  public Integer getMineLevelId() {
    return mineLevelId;
  }

  public void setMineLevelId(Integer mineLevelId) {
    this.mineLevelId = mineLevelId;
  }


  public String getNameZh() {
    return nameZh;
  }

  public void setNameZh(String nameZh) {
    this.nameZh = nameZh;
  }


  public String getNameEn() {
    return nameEn;
  }

  public void setNameEn(String nameEn) {
    this.nameEn = nameEn;
  }

}
