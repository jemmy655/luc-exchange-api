package ltd.trilobite.exchange.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.Table;
import ltd.trilobite.sdk.jdbc.IdType;

@Table(name = "maket_detail")
public class MaketDetail {
  @Id(type = IdType.Seq)
  private Long maketDetailId;
  private String symbol;
  private Long marketTypeId;


  public Long getMaketDetailId() {
    return maketDetailId;
  }

  public void setMaketDetailId(Long maketDetailId) {
    this.maketDetailId = maketDetailId;
  }


  public String getSymbol() {
    return symbol;
  }

  public void setSymbol(String symbol) {
    this.symbol = symbol;
  }


  public Long getMarketTypeId() {
    return marketTypeId;
  }

  public void setMarketTypeId(Long marketTypeId) {
    this.marketTypeId = marketTypeId;
  }

}
