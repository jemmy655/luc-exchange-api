package ltd.trilobite.exchange.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.Table;
import ltd.trilobite.sdk.jdbc.IdType;

@Table(name = "member_product")
public class MemberProduct {
  @Id(type = IdType.Seq)
  private Long memberProductId;
  private String name;
  private String price;
  private Integer onOff;
  private Long productTypeId;
  private Integer htmlHeight;
  private Long shopId;
  private String unitName;
  private Integer isGroupPay;


  public Long getMemberProductId() {
    return memberProductId;
  }

  public void setMemberProductId(Long memberProductId) {
    this.memberProductId = memberProductId;
  }


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }


  public String getPrice() {
    return price;
  }

  public void setPrice(String price) {
    this.price = price;
  }


  public Integer getOnOff() {
    return onOff;
  }

  public void setOnOff(Integer onOff) {
    this.onOff = onOff;
  }


  public Long getProductTypeId() {
    return productTypeId;
  }

  public void setProductTypeId(Long productTypeId) {
    this.productTypeId = productTypeId;
  }


  public Integer getHtmlHeight() {
    return htmlHeight;
  }

  public void setHtmlHeight(Integer htmlHeight) {
    this.htmlHeight = htmlHeight;
  }


  public Long getShopId() {
    return shopId;
  }

  public void setShopId(Long shopId) {
    this.shopId = shopId;
  }


  public String getUnitName() {
    return unitName;
  }

  public void setUnitName(String unitName) {
    this.unitName = unitName;
  }


  public Integer getIsGroupPay() {
    return isGroupPay;
  }

  public void setIsGroupPay(Integer isGroupPay) {
    this.isGroupPay = isGroupPay;
  }

}
