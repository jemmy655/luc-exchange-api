package ltd.trilobite.exchange.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.Table;
import ltd.trilobite.sdk.jdbc.IdType;

@Table(name = "person_certification")
public class PersonCertification {
  @Id
  private Long personId;
  private String realName;
  private String cardNo;
  private String cardBg;
  private String cardFront;
  private String handImg;
  private Integer isAudit;
  private String auditMessage;

  private String province;
  private String city;
  private String area;
  private String street;
  private Integer payState;
  private java.sql.Timestamp createTime;
  private java.sql.Timestamp updateTime;
  private Long auditPerson;
  private String price;
  private Long orderId;


  public String getRealName() {
    return realName;
  }

  public void setRealName(String realName) {
    this.realName = realName;
  }


  public String getCardNo() {
    return cardNo;
  }

  public void setCardNo(String cardNo) {
    this.cardNo = cardNo;
  }


  public String getCardBg() {
    return cardBg;
  }

  public void setCardBg(String cardBg) {
    this.cardBg = cardBg;
  }


  public String getCardFront() {
    return cardFront;
  }

  public void setCardFront(String cardFront) {
    this.cardFront = cardFront;
  }


  public String getHandImg() {
    return handImg;
  }

  public void setHandImg(String handImg) {
    this.handImg = handImg;
  }


  public Integer getIsAudit() {
    return isAudit;
  }

  public void setIsAudit(Integer isAudit) {
    this.isAudit = isAudit;
  }


  public String getAuditMessage() {
    return auditMessage;
  }

  public void setAuditMessage(String auditMessage) {
    this.auditMessage = auditMessage;
  }


  public Long getPersonId() {
    return personId;
  }

  public void setPersonId(Long personId) {
    this.personId = personId;
  }


  public String getProvince() {
    return province;
  }

  public void setProvince(String province) {
    this.province = province;
  }


  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }


  public String getArea() {
    return area;
  }

  public void setArea(String area) {
    this.area = area;
  }


  public String getStreet() {
    return street;
  }

  public void setStreet(String street) {
    this.street = street;
  }


  public Integer getPayState() {
    return payState;
  }

  public void setPayState(Integer payState) {
    this.payState = payState;
  }


  public java.sql.Timestamp getCreateTime() {
    return createTime;
  }

  public void setCreateTime(java.sql.Timestamp createTime) {
    this.createTime = createTime;
  }


  public java.sql.Timestamp getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(java.sql.Timestamp updateTime) {
    this.updateTime = updateTime;
  }


  public Long getAuditPerson() {
    return auditPerson;
  }

  public void setAuditPerson(Long auditPerson) {
    this.auditPerson = auditPerson;
  }


  public String getPrice() {
    return price;
  }

  public void setPrice(String price) {
    this.price = price;
  }


  public Long getOrderId() {
    return orderId;
  }

  public void setOrderId(Long orderId) {
    this.orderId = orderId;
  }

}
