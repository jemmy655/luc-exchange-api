package ltd.trilobite.exchange.dao.entry;

import ltd.trilobite.sdk.jdbc.Id;
import ltd.trilobite.sdk.jdbc.Table;
import ltd.trilobite.sdk.jdbc.IdType;

@Table(name = "person_miner_producte")
public class PersonMinerProducte {
  @Id(type = IdType.Seq)
  private Long personMinerProducteId;
  private Long personId;
  private Integer rewardsTypeId;
  private Double hourVal;
  private Long personMinerId;
  private Double coutVal;
  private Double alreadyVal;
  private Integer walletSuport;
  private Double usPrice;


  public Long getPersonMinerProducteId() {
    return personMinerProducteId;
  }

  public void setPersonMinerProducteId(Long personMinerProducteId) {
    this.personMinerProducteId = personMinerProducteId;
  }


  public Long getPersonId() {
    return personId;
  }

  public void setPersonId(Long personId) {
    this.personId = personId;
  }


  public Integer getRewardsTypeId() {
    return rewardsTypeId;
  }

  public void setRewardsTypeId(Integer rewardsTypeId) {
    this.rewardsTypeId = rewardsTypeId;
  }


  public Double getHourVal() {
    return hourVal;
  }

  public void setHourVal(Double hourVal) {
    this.hourVal = hourVal;
  }


  public Long getPersonMinerId() {
    return personMinerId;
  }

  public void setPersonMinerId(Long personMinerId) {
    this.personMinerId = personMinerId;
  }


  public Double getCoutVal() {
    return coutVal;
  }

  public void setCoutVal(Double coutVal) {
    this.coutVal = coutVal;
  }


  public Double getAlreadyVal() {
    return alreadyVal;
  }

  public void setAlreadyVal(Double alreadyVal) {
    this.alreadyVal = alreadyVal;
  }


  public Integer getWalletSuport() {
    return walletSuport;
  }

  public void setWalletSuport(Integer walletSuport) {
    this.walletSuport = walletSuport;
  }


  public Double getUsPrice() {
    return usPrice;
  }

  public void setUsPrice(Double usPrice) {
    this.usPrice = usPrice;
  }

}
