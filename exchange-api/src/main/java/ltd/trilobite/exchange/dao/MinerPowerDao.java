package ltd.trilobite.exchange.dao;


import ltd.trilobite.exchange.dao.entry.MinerPower;
import ltd.trilobite.exchange.dao.entry.MinerPrice;
import ltd.trilobite.sdk.factory.App;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;
import ltd.trilobite.sdk.status.Result;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;

public class MinerPowerDao extends BaseDao<MinerPower>{


    public List<Map<String,Object>> findList(MinerPower minerPower){
        JdbcTemplet jdbcTemplet = App.get("master");
        String sql="select mp.miner_power_id,mp.miner_id,mp.miner_power_type,mp.day_hour,mpt.name_en,mpt.name_zh,mpt.free_num,mpt.rewards_type_id,t.name_zh as t_name_zh,t.name_en as t_name_en from (select  * from miner_power where miner_id=?) mp" +
                " left join miner_power_type mpt on mpt.miner_power_type=mp.miner_power_type" +
                " left join rewards_type t on mpt.rewards_type_id = t.rewards_type_id";
        return jdbcTemplet.findList(sql,minerPower.getMinerId());
    }

}
