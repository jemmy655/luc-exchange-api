package ltd.trilobite.exchange.service;

import ltd.trilobite.exchange.dao.*;
import ltd.trilobite.exchange.dao.entry.*;
import ltd.trilobite.sdk.status.Result;


import java.util.*;

public class PersonMinerService {
    PersonMinerDao personMinerDao = new PersonMinerDao();
    MinerPriceDao minerPriceDao=new MinerPriceDao();
    Map<String,Object> order=new HashMap<>();
    BillDao billDao=new BillDao();
    RewardsBillDao rewardsBillDao=new RewardsBillDao();
    PayPersonDao payPersonDao=new PayPersonDao();
    MinerDao minerDao=new MinerDao();
    MinerProduceDao minerProduce=new MinerProduceDao();
    RewardsTypePriceDao rewardsTypePriceDao=new RewardsTypePriceDao();
    PersonMinerProducteDao personMinerProducteDao=new PersonMinerProducteDao();
    /**
     * 请求支付
     * @param minerId
     * @param personId
     * @return
     */
    public Result query(String minerId,Long personId,String walletType,Integer num){
        Map<String,Object> result=new HashMap<String,Object>();
        String uid= UUID.randomUUID().toString();
        Miner param=new Miner();
        param.setMinerId(Long.parseLong(minerId));
        Miner mpResult=minerDao.findOne(param,Miner.class);
        MinerPrice minerPrice=new MinerPrice();
        minerPrice.setMinerId(param.getMinerId());
        result.put("product",mpResult);
        result.put("wallettype",walletType);
        result.put("num",num);
        if(walletType.trim().equals("-1")) {
            result.put("price_list", minerPriceDao.findBillBalanceList(param.getMinerId(), personId));
        }else  if(walletType.trim().equals("-2")) {
            result.put("price_list", minerPriceDao.findRewardsBillBalanceList(param.getMinerId(), personId));
        }else {
            result.put("price_list", minerPriceDao.findMinerWalletBalanceList(param.getMinerId(), personId,Integer.parseInt(walletType)));

        }
        result.put("uid",uid);
        order.put(uid,result);
        new Thread(()->{
            try {
                Thread.sleep(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            order.remove(uid);
        }).start();
        return new Result(result);
    }

    public Result checkOut(String uid,Long personId,String rewardsTypeId){
        if(!order.containsKey(uid)){
            return new Result(-2);
        }
//        if(!payPersonDao.isPlayUser(form.get("password").toString(),personId)){
//            return new Result(-3);
//        }

        Map<String,Object> order1=(Map<String,Object>)order.get(uid);
        List<Map<String,Object>> priceList= (List<Map<String, Object>>) order1.get("price_list");
        String walledType=order1.get("wallettype").toString();
        Miner memberProduct= (Miner) order1.get("product");
        Integer num= (Integer) order1.get("num");
        if(memberProduct.getOnOff()==0){
            return new Result(-6); //矿机下架，无法购买
        }
        if(memberProduct.getBuyGoup()==0){
            if(rewardsTypeId.trim().equals("")){
                return new Result(-4);
            }
            List<Map<String,Object>> temp=new ArrayList<>();
            for(Map<String,Object> row:priceList){
                if(row.get("rewardsTypeId").toString().trim().equals(rewardsTypeId.trim())){
                    temp.add(row);
                }

            }
            if(temp.size()==0){
                return new Result(-5);
            }
            priceList= temp;
        }

        boolean bl=true;
        for(Map<String,Object> row:priceList){
            if(Double.parseDouble(row.get("price").toString())*num>Double.parseDouble(row.get("balance").toString())){
                bl=false;
            }
        }

        if(bl) {

            //如果是余额钱包扣余额
            if(walledType.trim().equals("-1")) {
                List<Object> bills=new ArrayList<>();
                for (Map<String, Object> row : priceList) {
                    Bill bill = new Bill();
                    bill.setRewardsTypeId(Integer.parseInt(row.get("rewardsTypeId").toString()));
                    bill.setNum(-Double.parseDouble(row.get("price").toString())*num);
                    bill.setDescription("购买矿机/Buy mining machine");
                    bill.setRewardsActionId(7);
                    bill.setPersonId(personId);
                    bill.setState(3);
                    bill.setSrcId(memberProduct.getMinerId());
                    bills.add(bill);
                }
                billDao.adds(bills);
            }else if(walledType.trim().equals("-2")) {
                List<Object> bills=new ArrayList<>();
                for (Map<String, Object> row : priceList) {
                    RewardsBill bill = new RewardsBill();
                    bill.setRewardsTypeId(Integer.parseInt(row.get("rewardsTypeId").toString()));
                    bill.setNum(-Double.parseDouble(row.get("price").toString())*num);
                    bill.setDescription("购买矿机/Buy mining machine");
                    bill.setRewardsActionId(7);
                    bill.setPersonId(personId);
                    bill.setState(3);
                    bill.setSrcId(memberProduct.getMinerId());
                    bills.add(bill);
                }
                rewardsBillDao.adds(bills);
            }else {
                List<Object> minerWallectBills=new ArrayList<>();
                for (Map<String, Object> row : priceList) {
                    MinerWallet bill = new MinerWallet();
                    bill.setRewardsTypeId(Integer.parseInt(row.get("rewardsTypeId").toString()));
                    bill.setNum(-Double.parseDouble(row.get("price").toString())*num);
                    bill.setDescription("购买矿机/Buy mining machine");
                    bill.setRewardsActionId(7);
                    bill.setPersonId(personId);
                    bill.setWalletSuport(Integer.parseInt(walledType));
                    //bill.setMinerWalletId();
                    //bill.setPersonMinerId(memberProduct.getMinerId());
                  //  bill.setState(3);
                   // bill.setSrcId(memberProduct.getMinerId());
                    minerWallectBills.add(bill);
                }
                rewardsBillDao.adds(minerWallectBills);
            }

            for(int i=0;i<num;i++) {
                addMine(memberProduct.getMinerId(), personId);
            }

            //加入矿机配置
            order.remove(uid);
            return new Result(200);
        }else{
            return new Result(-1);//余额不足
        }
    }

    /**
     * 加入矿机并设置个人配置项目
     * @param minerId
     * @param personId
     */
    public void addMine(Long minerId,Long personId){
        Miner minerParam=new Miner();
        minerParam.setMinerId(minerId);
        Miner miner=minerDao.findOne(minerParam,Miner.class);
        PersonMiner personMiner=new PersonMiner();
        personMiner.setPersonMinerId(personMinerDao.getSeq("person_miner"));
        personMiner.setMinerId(minerId);
        personMiner.setPersonId(personId);
        personMiner.setMaxRunHour(miner.getRunHour());
        personMiner.setUseHour(0);
        personMinerDao.add(personMiner);
        MinerProduce param=new MinerProduce();
        param.setMinerId(minerId);
        List<MinerProduce> mplist= minerProduce.list(param,MinerProduce.class);
        List<Object> li=new ArrayList<>();
        for(MinerProduce minerProduce:mplist) {
            PersonMinerProducte pmp=new PersonMinerProducte();
            RewardsTypePrice p=new RewardsTypePrice();
            p.setRewardsTypeId(minerProduce.getRewardsTypeId());
            RewardsTypePrice rewardsTypePrice= rewardsTypePriceDao.findOne(p,RewardsTypePrice.class);
            pmp.setPersonId(personId);
            pmp.setHourVal(minerProduce.getHourVal());
            pmp.setCoutVal(minerProduce.getCoutVal());
            pmp.setUsPrice(rewardsTypePrice.getUsPrice());
            pmp.setWalletSuport(minerProduce.getWalletSuport());
            pmp.setAlreadyVal(0d);
            pmp.setRewardsTypeId(minerProduce.getRewardsTypeId());
            pmp.setPersonMinerId(personMiner.getPersonMinerId());
            li.add(pmp);
        }
        personMinerProducteDao.adds(li);
    }
}
