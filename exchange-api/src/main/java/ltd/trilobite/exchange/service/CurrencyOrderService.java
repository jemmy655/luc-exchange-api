package ltd.trilobite.exchange.service;

import ltd.trilobite.exchange.dao.*;
import ltd.trilobite.exchange.dao.entry.*;
import ltd.trilobite.sdk.status.Result;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * C2C交易订单服务
 */
public class CurrencyOrderService {

    CurrencyOrderDao currencyOrderDao=new CurrencyOrderDao();
    CurrencyMarketDao currencyMarketDao=new CurrencyMarketDao();
    Map<String,Object> cache=new HashMap<>();
    RewardsTypeDao rewardsTypeDao=new RewardsTypeDao();
    CurrencyMarketWalletDao currencyMarketWalletDao=new CurrencyMarketWalletDao();
    PayPersonDao payPersonDao=new PayPersonDao();
    BillDao billDao=new BillDao();
    /**
     * 我要买
     * 创建消费者订单
     * @param order
     * @return
     */
    public Result createClientOrder(CurrencyOrder order){
        CurrencyMarket currencyMarketParam=new CurrencyMarket();
        currencyMarketParam.setCurrencyMarketId(order.getCurrencyMarketId());
        CurrencyMarket currencyMarket =currencyMarketDao.findOne(currencyMarketParam,CurrencyMarket.class);
        if(currencyMarket!=null) {
            String uid = UUID.randomUUID().toString();
            order.setToken(uid);
            //设置商家的编号
            order.setSellId(currencyMarket.getPersonId());
            order.setState(1); //待处理
            if(order.getNum()<currencyMarket.getNumMin()){
               return new Result(-2);//至少需要购买到设定的数量
            }
            if(order.getNum()>currencyMarket.getNumMax()){
                return new Result(-3);//最多需要购买到设定的数量
            }

            CurrencyMarket updateParam=new CurrencyMarket();
            Double blnum= currencyMarket.getNumMax()-order.getNum();//剩余数量
            updateParam.setNumMax(blnum);
            //如果剩余数量小于允许最小的数量，最小的数量等于剩余数量
            if(blnum<currencyMarket.getNumMin()){
                currencyMarket.setNumMin(blnum);
            }
            currencyOrderDao.add(order);
            currencyMarketDao.update(updateParam);
        }else
        {
            return new Result(-1);//无市场信息，无法创建订单
        }
        return  new Result(200);
    }

    /**
     * 确认商家的订单
     * @param token
     * @return
     */

    public Result confirmMerchantOrder(String token){
        Map<String,Object> map= (Map<String, Object>) cache.get(token);
        CurrencyMarket currencyMarket= (CurrencyMarket) map.get("currencyMarket");
        CurrencyOrder order= (CurrencyOrder) map.get("order");
        order.setCurrencyOrder(billDao.getSeq("currency_order"));
        CurrencyMarket updateParam=new CurrencyMarket();
        Double blnum= currencyMarket.getNumMax()-order.getNum();//剩余数量
        updateParam.setNumMax(blnum);
        updateParam.setCurrencyMarketId(currencyMarket.getCurrencyMarketId());
        //如果剩余数量小于允许最小的数量，最小的数量等于剩余数量
        if(blnum<currencyMarket.getNumMin()){
            updateParam.setNumMin(blnum);
        }
        Bill bill=new Bill();
        bill.setPersonId(order.getSellId());
        bill.setSrcId(order.getCurrencyOrder());
        bill.setRewardsActionId(11);
        bill.setRewardsTypeId(currencyMarket.getRewardsTypeId());
        bill.setNum(order.getNum());
        bill.setDescription("场外交易订单/OTC orders");
        billDao.add(bill);
        CurrencyMarketWallet currencyMarketWallet=new CurrencyMarketWallet();
        currencyMarketWallet.setPersonId(order.getSellId());
        currencyMarketWallet.setCurrencyMarketId(currencyMarket.getCurrencyMarketId());
        currencyMarketWallet.setNum(order.getNum());
        currencyMarketWallet.setRewardsTypeId(currencyMarket.getRewardsTypeId());
        currencyMarketWalletDao.add(currencyMarketWallet);

        currencyOrderDao.add(order);
        currencyMarketDao.update(updateParam);
        cache.remove(token);
        return  new Result(200);
    }

    /**
     * 确认支付
     * @param param
     * @return
     */
    public Result confirmPay(CurrencyOrder param){
        CurrencyOrder order=currencyOrderDao.findOne(param,CurrencyOrder.class);
        order.setState(2);
        if(param.getSellId()==order.getSellId()){
            currencyOrderDao.update(order);
            return new Result(200);
        }
        return new Result(-1);//不是商家无法确认支付
    }

    /**
     * 确认放行
     * @param param
     * @param password
     * @return
     */
    public Result confirmRelease(CurrencyOrder param,String password){
        CurrencyOrder order=currencyOrderDao.findOne(param,CurrencyOrder.class);
        if(order.getState()!=2){
            return new Result(-2);//订单状态必须是已支付
        }
        if(param.getSellId()!=order.getSellId()){
            return new Result(-1);
        }
        if(payPersonDao.isPlayUser(password,order.getSellId())){
            return  new Result(-3);//支付密码不对
        }
        CurrencyMarket currencyMarketParam=new CurrencyMarket();
        currencyMarketParam.setCurrencyMarketId(order.getCurrencyMarketId());
        CurrencyMarket currencyMarket =currencyMarketDao.findOne(currencyMarketParam,CurrencyMarket.class);



        CurrencyMarketWallet currencyMarketWallet=new CurrencyMarketWallet();
        currencyMarketWallet.setPersonId(order.getSellId());
        currencyMarketWallet.setCurrencyMarketId(currencyMarket.getCurrencyMarketId());
        currencyMarketWallet.setNum(-order.getNum());//扣除商家的货币
        currencyMarketWallet.setRewardsTypeId(currencyMarket.getRewardsTypeId());
        currencyMarketWalletDao.add(currencyMarketWallet);

        Bill bill=new Bill();
        bill.setPersonId(order.getBuyId());
        bill.setSrcId(order.getCurrencyOrder());
        bill.setRewardsActionId(11);
        bill.setRewardsTypeId(currencyMarket.getRewardsTypeId());
        bill.setNum(order.getNum());
        bill.setDescription("场外交易订单/OTC orders");
        billDao.add(bill);
        if(currencyMarket.getNumMin()==0){
            currencyMarket.setOnOff(2);//挂单需求已满足，结束挂单
            currencyMarketDao.update(currencyMarket);
        }

        order.setState(3);//已完成
        currencyOrderDao.update(order);
        return new Result(200);//不是商家无法确认支付
    }

    /**
     * 我要卖
     * 创建商家订单
     * @param order
     * @return
     */
    public Result queryMerchantOrder(CurrencyOrder order){
        CurrencyMarket currencyMarketParam=new CurrencyMarket();
        currencyMarketParam.setCurrencyMarketId(order.getCurrencyMarketId());
        CurrencyMarket currencyMarket =currencyMarketDao.findOne(currencyMarketParam,CurrencyMarket.class);
        String uid = UUID.randomUUID().toString();
        if(currencyMarket!=null) {

            order.setToken(uid);
            //设置商家的编号
            order.setBuyId(currencyMarket.getPersonId());
            order.setState(1); //待处理
            if(order.getNum()<currencyMarket.getNumMin()){
                return new Result(-2);//至少需要购买到设定的数量
            }
            if(order.getNum()>currencyMarket.getNumMax()){
                return new Result(-3);//最多需要购买到设定的数量
            }


            RewardsType rewardsType=new RewardsType();
            rewardsType.setRewardsTypeId(currencyMarket.getRewardsTypeId());
            Map<String,Object> map=new HashMap<>();
            map.put("currencyMarket",currencyMarket);
            map.put("rewardsType", rewardsTypeDao.findOne(rewardsType, RewardsType.class));
            map.put("order",order);

            Double balance=billDao.balance(order.getSellId(),currencyMarket.getRewardsTypeId());
            if(balance<currencyMarket.getNumMin()){
                return new Result(-3);//余额不足无法支付
            }
            cache.put(uid,map);
            new Thread(()->{
                try {
                    Thread.sleep(30000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                cache.remove(uid);
            }).start();
        }else
        {
            return new Result(-1);//无市场信息，无法创建订单
        }
        return  new Result(200,uid);
    }
}
