package ltd.trilobite.exchange.api.miner;

import ltd.trilobite.adminproxy.AdminAuthProxy;
import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.AuthProxy;
import ltd.trilobite.exchange.api.BaseAction;
import ltd.trilobite.exchange.dao.*;
import ltd.trilobite.exchange.dao.entry.*;
import ltd.trilobite.exchange.service.PersonMinerService;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;



@ApiDesc(desc = "2002-我的矿机")
@RestService
public class PersonMinerAction extends BaseAction<PersonMiner> {
    PersonMinerDao personMinerDao = new PersonMinerDao();

    PersonMinerService personMinerService=new PersonMinerService();
    @ApiDoc(title = "拉取支付", param = {
            @ApiDesc(name = "minerId", type = "string", desc = "矿机编号"),
            @ApiDesc(name = "walledType", type = "string", desc = "钱包类型"),
            @ApiDesc(name = "num", type = "string", desc = "购买数量"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/personMiner/queryPay")
    @Proxy(target = AuthProxy.class)
    public Result queryPay(RestForm form) {
        Long personId=Long.parseLong(form.getHeader().get("personId"));
        String minerId=form.get("minerId");
        return personMinerService.query(minerId,personId,form.get("walledType"),Integer.parseInt(form.get("num")));
    }

    @ApiDoc(title = "支付", param = {
            @ApiDesc(name = "uid", type = "string", desc = "产品编号"),
            @ApiDesc(name = "rewardsTypeId", type = "string", desc = "支付类型"),
//            @ApiDesc(name = "password", type = "string", desc = "支付密码"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "200", type = "string", desc = "成功"),
                    @ApiDesc(name = "-1", type = "string", desc = "余额不够"),
                    @ApiDesc(name = "-2", type = "string", desc = "支付超时"),
                    @ApiDesc(name = "-3", type = "string", desc = "支付密码不对"),
                    @ApiDesc(name = "-4", type = "string", desc = "单选模式下支付类型不能为空"),
                    @ApiDesc(name = "-5", type = "string", desc = "未找到支付类型"),
                    @ApiDesc(name = "-6", type = "string", desc = "矿机停止售卖，无法购买"),
            }

    )
    @Router(path = "/personMiner/checkOut")
    @Proxy(target = AuthProxy.class)
    public Result checkOut(RestForm form) {
       Long personId=Long.parseLong(form.getHeader().get("personId"));
       return personMinerService.checkOut(form.get("uid"),personId,form.get("rewardsTypeId"));
    }



    @Router(path = "/personMiner/update")
    @Proxy(target = AuthProxy.class)
    public Result update(RestForm form, @Param PersonMiner personMiner) {
        return super.update1(personMinerDao, personMiner);
    }

    @Router(path = "/personMiner/delete")
    @Proxy(target = AuthProxy.class)
    public Result delete(RestForm form, @Param PersonMiner personMiner) {
        return super.delete1(personMinerDao, personMiner);
    }

    @Router(path = "/personMiner/findOne")
    @Proxy(target = AuthProxy.class)
    public Result findOne(RestForm form, @Param PersonMiner personMiner) {
        return super.findOne1(personMinerDao, personMiner, PersonMiner.class);
    }

    @ApiDoc(title = "我的矿机", param = {
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/personMiner/perList")
    @Proxy(target = AuthProxy.class)
    public Result perList(RestForm form, @Param PersonMiner personMiner) {
        return personMinerDao.perlist(form,form.getHeader().get("personId"));
    }

    @ApiDoc(title = "查询用户可购买数量和允许购买数量", param = {
            @ApiDesc(name = "minerId", type = "string", desc = "矿机编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/personMiner/findBuyNum")
    @Proxy(target = AuthProxy.class)
    public Result findBuyNum(RestForm form, @Param PersonMiner personMiner) {
        return new Result(personMinerDao.findBuyNum(form.getHeader().get("personId"),form.get("minerId")));
    }



}
