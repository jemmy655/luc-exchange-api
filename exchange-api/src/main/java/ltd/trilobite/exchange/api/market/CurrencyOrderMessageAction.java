package ltd.trilobite.exchange.api.market;

import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.AuthProxy;
import ltd.trilobite.exchange.api.BaseAction;
import ltd.trilobite.exchange.dao.CurrencyOrderMessageDao;
import ltd.trilobite.exchange.dao.entry.CurrencyOrderMessage;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;

@ApiDesc(desc = "4004-C2C交易留言信息")
@RestService
public class CurrencyOrderMessageAction extends BaseAction<CurrencyOrderMessage> {
    CurrencyOrderMessageDao currencyOrderMessageDao = new CurrencyOrderMessageDao();
    @ApiDoc(title = "新增", param = {
            @ApiDesc(name = "currencyOrderMessageId", type = "string", desc = "编号"),
            @ApiDesc(name = "message", type = "string", desc = "消息内容"),
            @ApiDesc(name = "cType", type = "string", desc = "类型"),
            @ApiDesc(name = "currencyOrder", type = "string", desc = "货币订单编号"),
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/currencyOrderMessage/add")
    @Proxy(target = AuthProxy.class)
    public Result add(RestForm form, @Param CurrencyOrderMessage currencyOrderMessage) {
        Long personId=Long.parseLong(form.getHeader().get("personId"));
        currencyOrderMessage.setPersonId(personId);
        return super.add1(currencyOrderMessageDao, currencyOrderMessage);
    }
//    @ApiDoc(title = "修改", param = {
//            @ApiDesc(name = "currencyOrderMessageId", type = "string", desc = "编号"),
//            @ApiDesc(name = "message", type = "string", desc = "消息内容"),
//            @ApiDesc(name = "cType", type = "string", desc = "类型"),
//            @ApiDesc(name = "currencyOrder", type = "string", desc = "货币订单编号"),
//    },
//            result = {
//                    @ApiDesc(name = "true", type = "string", desc = "成功"),
//            }
//
//    )
//    @Router(path = "/currencyOrderMessage/update")
//    @Proxy(target = AuthProxy.class)
//    public Result update(RestForm form, @Param CurrencyOrderMessage currencyOrderMessage) {
//        return super.update1(currencyOrderMessageDao, currencyOrderMessage);
//    }

    @ApiDoc(title = "分页查询", param = {
            @ApiDesc(name = "pageSize", type = "string", desc = "分页大小"),
            @ApiDesc(name = "start", type = "string", desc = "开始行"),
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/currencyOrderMessage/perList")
    @Proxy(target = AuthProxy.class)
    public Result perList(RestForm form, @Param CurrencyOrderMessage currencyOrderMessage) {
        return currencyOrderMessageDao.perlist(form);
    }

}
