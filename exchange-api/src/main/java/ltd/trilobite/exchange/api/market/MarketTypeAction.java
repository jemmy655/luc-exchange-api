package ltd.trilobite.exchange.api.market;

import ltd.trilobite.adminproxy.AdminAuthProxy;
import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.AuthProxy;
import ltd.trilobite.exchange.api.BaseAction;
import ltd.trilobite.exchange.dao.MarketTypeDao;
import ltd.trilobite.exchange.dao.entry.MarketType;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;

@ApiDesc(desc = "4002-市场类型")
@RestService
public class MarketTypeAction extends BaseAction<MarketType> {
    MarketTypeDao marketTypeDao = new MarketTypeDao();

    @Router(path = "/marketType/add")
    @Proxy(target = AdminAuthProxy.class)
    public Result add(RestForm form, @Param MarketType marketType) {
        return super.add1(marketTypeDao, marketType);
    }

    @Router(path = "/marketType/update")
    @Proxy(target = AdminAuthProxy.class)
    public Result update(RestForm form, @Param MarketType marketType) {
        return super.update1(marketTypeDao, marketType);
    }

    @Router(path = "/marketType/delete")
    @Proxy(target = AdminAuthProxy.class)
    public Result delete(RestForm form, @Param MarketType marketType) {
        return super.delete1(marketTypeDao, marketType);
    }

    @Router(path = "/marketType/findOne")
    @Proxy(target = AdminAuthProxy.class)
    public Result findOne(RestForm form, @Param MarketType marketType) {
        return super.findOne1(marketTypeDao, marketType, MarketType.class);
    }


    @ApiDoc(title = "市场分类", param = {
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/marketType/findList")
    @Proxy(target = AuthProxy.class)
    public Result findList(RestForm form, @Param MarketType marketType) {
        return marketTypeDao.findList(form, marketType, MarketType.class);
    }

}
