package ltd.trilobite.exchange.api.miner;

import ltd.trilobite.adminproxy.AdminAuthProxy;
import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.exchange.api.BaseAction;
import ltd.trilobite.exchange.dao.MinerPowerDao;
import ltd.trilobite.exchange.dao.entry.MinerPower;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;

@ApiDesc(desc = "2004-矿机算力构成设置")
@RestService
public class MinerPowerAction extends BaseAction<MinerPower> {

    MinerPowerDao minerPowerDao = new MinerPowerDao();
    @ApiDoc(title = "新增", param = {
            @ApiDesc(name = "minerPowerId", type = "string", desc = "编号"),
            @ApiDesc(name = "minerId", type = "string", desc = "矿机编号"),
            @ApiDesc(name = "minerPowerType", type = "string", desc = "能量类型"),
            @ApiDesc(name = "dayNum", type = "string", desc = "每小时消耗能量"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "200", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/minerPower/add")
    @Proxy(target = AdminAuthProxy.class)
    public Result add(RestForm form, @Param MinerPower minerPower) {
        return super.add1(minerPowerDao, minerPower);
    }
    @ApiDoc(title = "修改", param = {
            @ApiDesc(name = "minerPowerId", type = "string", desc = "编号"),
            @ApiDesc(name = "minerId", type = "string", desc = "矿机编号"),
            @ApiDesc(name = "minerPowerType", type = "string", desc = "能量类型"),
            @ApiDesc(name = "hourNum", type = "string", desc = "每小时消耗能量"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "200", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/minerPower/update")
    @Proxy(target = AdminAuthProxy.class)
    public Result update(RestForm form, @Param MinerPower minerPower) {
//        minerPower.setMinerId(null);
//        minerPower.setMinerPowerId(null);
        return super.update1(minerPowerDao, minerPower);
    }

    @ApiDoc(title = "删除", param = {
            @ApiDesc(name = "minerPowerId", type = "string", desc = "编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "200", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/minerPower/delete")
    @Proxy(target = AdminAuthProxy.class)
    public Result delete(RestForm form, @Param MinerPower minerPower) {
        return super.delete1(minerPowerDao, minerPower);
    }

    @ApiDoc(title = "查询列表", param = {
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "200", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/minerPower/findList")
    @Proxy(target = AdminAuthProxy.class)
    public Result findList(RestForm form, @Param MinerPower minerPower) {
        return new Result(minerPowerDao.findList(minerPower));
    }
    @ApiDoc(title = "分页查询", param = {

            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "200", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/minerPower/perList")
    @Proxy(target = AdminAuthProxy.class)
    public Result perList(RestForm form, @Param MinerPower minerPower) {
        return super.perList1(minerPowerDao, form, minerPower, MinerPower.class);
    }



}
