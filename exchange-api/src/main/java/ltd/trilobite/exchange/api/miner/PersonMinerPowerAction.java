package ltd.trilobite.exchange.api.miner;

import ltd.trilobite.adminproxy.AdminAuthProxy;
import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.AuthProxy;
import ltd.trilobite.exchange.api.BaseAction;
import ltd.trilobite.exchange.dao.PersonMinerPowerDao;
import ltd.trilobite.exchange.dao.entry.MinerPower;
import ltd.trilobite.exchange.dao.entry.PersonMinerPower;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;

@ApiDesc(desc = "2010-我的能量")
@RestService
public class PersonMinerPowerAction extends BaseAction<PersonMinerPower> {
    PersonMinerPowerDao personMinerPowerDao = new PersonMinerPowerDao();

    @ApiDoc(title = "今日能量", param = {

            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "200", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/minerPower/dayPower")
    @Proxy(target = AuthProxy.class)
    public Result dayPower(RestForm form, @Param MinerPower minerPower) {
        Long personId = Long.parseLong(form.getHeader().get("personId"));
        return new Result(personMinerPowerDao.dayPower(personId));
    }

    @ApiDoc(title = "昨日能量", param = {

            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "200", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/minerPower/prvDayPower")
    @Proxy(target = AuthProxy.class)
    public Result prvPower(RestForm form, @Param MinerPower minerPower) {
        Long personId = Long.parseLong(form.getHeader().get("personId"));
        return new Result(personMinerPowerDao.prvDayPower(personId));
    }
}
