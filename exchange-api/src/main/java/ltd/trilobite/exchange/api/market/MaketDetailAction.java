package ltd.trilobite.exchange.api.market;

import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.AuthProxy;
import ltd.trilobite.exchange.api.BaseAction;
import ltd.trilobite.exchange.dao.MaketDetailDao;
import ltd.trilobite.exchange.dao.entry.MaketDetail;
import ltd.trilobite.exchange.dao.entry.MarketInfo;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;

@ApiDesc(desc = "4003-市场详情")
@RestService
public class MaketDetailAction extends BaseAction<MaketDetail> {
    MaketDetailDao maketDetailDao = new MaketDetailDao();

    @Router(path = "/maketDetail/add")
    @Proxy(target = AuthProxy.class)
    public Result add(RestForm form, @Param MaketDetail maketDetail) {
        return super.add1(maketDetailDao, maketDetail);
    }

    @Router(path = "/maketDetail/put")
    public Result put(RestForm form, @Param MarketInfo info) {
         maketDetailDao.addPool(info);
         return new Result(200);
    }



    @Router(path = "/maketDetail/update")
    @Proxy(target = AuthProxy.class)
    public Result update(RestForm form, @Param MaketDetail maketDetail) {
        return super.update1(maketDetailDao, maketDetail);
    }

    @Router(path = "/maketDetail/delete")
    @Proxy(target = AuthProxy.class)
    public Result delete(RestForm form, @Param MaketDetail maketDetail) {
        return super.delete1(maketDetailDao, maketDetail);
    }

    @Router(path = "/maketDetail/findOne")
    @Proxy(target = AuthProxy.class)
    public Result findOne(RestForm form, @Param MaketDetail maketDetail) {
        return super.findOne1(maketDetailDao, maketDetail, MaketDetail.class);
    }

    @ApiDoc(title = "市场列表信息分页", param = {
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/maketDetail/perList")
//    @Proxy(target = AuthProxy.class)
    public Result perList(RestForm form, @Param MaketDetail maketDetail) {
        return maketDetailDao.perList(form);
    }

}
