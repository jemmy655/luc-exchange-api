package ltd.trilobite.exchange.api.financial;

import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.AuthProxy;
import ltd.trilobite.exchange.api.BaseAction;
import ltd.trilobite.exchange.dao.*;
import ltd.trilobite.exchange.dao.entry.*;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.factory.App;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;
import ltd.trilobite.sdk.util.Util;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@ApiDesc(desc = "3002-保险")
@RestService
public class InsuranceAction extends BaseAction<Insurance> {
    InsuranceDao insuranceDao = new InsuranceDao();
    ShopDao shopDao = new ShopDao();
    RewardsTypeDao rewardsTypeDao = new RewardsTypeDao();
    PayPersonDao payPersonDao = new PayPersonDao();
    BillDao billDao = new BillDao();
    InsurancePersonDao insurancePersonDao=new InsurancePersonDao();
    Map<String, Object> order = new HashMap<>();

    @ApiDoc(title = "拉取支付", param = {
            @ApiDesc(name = "insuranceId", type = "string", desc = "保险产品编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/insurance/queryPay")
    @Proxy(target = AuthProxy.class)
    public Result queryPay(RestForm form) {
        Long personId = Long.parseLong(form.getHeader().get("personId"));
        Map<String, Object> result = new HashMap<String, Object>();
        String uid = UUID.randomUUID().toString();
        String productId = form.get("insuranceId");
        Insurance insuranceParam = new Insurance();
        insuranceParam.setInsuranceId(Long.parseLong(productId));
        Insurance insurance = insuranceDao.findOne(insuranceParam, Insurance.class);
        Shop shopParam = new Shop();
        shopParam.setShopId(insurance.getShopId());
        Shop shop = shopDao.findOne(shopParam, Shop.class);
        RewardsType rewardsTypeParam = new RewardsType();
        rewardsTypeParam.setRewardsTypeId(insurance.getRewardsTypeId());
        RewardsType rewardsType = rewardsTypeDao.findOne(rewardsTypeParam, RewardsType.class);
        result.put("shop", shop);
        result.put("insurance", insurance);
        result.put("rewardType", rewardsType);
        result.put("balance", billDao.balance(personId, insurance.getRewardsTypeId()));
        result.put("uid", uid);
        order.put(uid, result);
        new Thread(() -> {
            try {
                Thread.sleep(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            order.remove(uid);
        }).start();
        return new Result(result);
    }


    @ApiDoc(title = "支付", param = {
            @ApiDesc(name = "uid", type = "string", desc = "支付编号"),
            @ApiDesc(name = "password", type = "string", desc = "支付密码"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "200", type = "string", desc = "成功"),
                    @ApiDesc(name = "-2", type = "string", desc = "支付超时"),
                    @ApiDesc(name = "-3", type = "string", desc = "支付密码不对"),
            }

    )
    @Router(path = "/insurance/checkOut")
    @Proxy(target = AuthProxy.class)
    public Result checkOut(RestForm form) {
        String uid = form.get("uid");
        Long personId = Long.parseLong(form.getHeader().get("personId"));

        if (!order.containsKey(uid)) {
            return new Result(-2);
        }
        if (!payPersonDao.isPlayUser(form.get("password").toString(), personId)) {
            return new Result(-3);
        }

        Map<String, Object> order1 = (Map<String, Object>) order.get(uid);
        Insurance insurance = (Insurance) order1.get("insurance");

        Bill bill = new Bill();
        bill.setRewardsTypeId(insurance.getRewardsTypeId());
        bill.setNum(insurance.getPrice());
        bill.setDescription("购买保险项目");
        bill.setRewardsActionId(7);
        bill.setPersonId(personId);
       // bill.setActionCode("insurance");
        bill.setState(3);
        bill.setSrcId(insurance.getInsuranceId());
        billDao.add(bill);
        InsurancePerson insurancePerson=new InsurancePerson();
        insurancePerson.setPersonId(personId);
        insurancePerson.setInsuranceId(insurance.getInsuranceId());
        insurancePerson.setStartState(1);
        insurancePerson.setStartTime(Timestamp.valueOf(LocalDateTime.now()));
        Calendar c = Calendar.getInstance();
        c.add(Calendar.YEAR,insurance.getValTime());
        insurancePerson.setEndTime(Timestamp.valueOf(LocalDateTime.ofInstant(c.toInstant(),c.getTimeZone().toZoneId())));
        insurancePersonDao.add(insurancePerson);
        order.remove(uid);
        return new Result(200);


    }
    @Router(path = "/insurance/add")
    @Proxy(target = AuthProxy.class)
    public Result add(RestForm form, @Param Insurance insurance) {
        return super.add1(insuranceDao, insurance);
    }

    @Router(path = "/insurance/update")
    @Proxy(target = AuthProxy.class)
    public Result update(RestForm form, @Param Insurance insurance) {
        return super.update1(insuranceDao, insurance);
    }

    @Router(path = "/insurance/delete")
    @Proxy(target = AuthProxy.class)
    public Result delete(RestForm form, @Param Insurance insurance) {
        return super.delete1(insuranceDao, insurance);
    }

    @Router(path = "/insurance/findOne")
    @Proxy(target = AuthProxy.class)
    public Result findOne(RestForm form, @Param Insurance insurance) {
        return super.findOne1(insuranceDao, insurance, Insurance.class);
    }

    @Router(path = "/insurance/perList")
    @Proxy(target = AuthProxy.class)
    public Result perList(RestForm form, @Param Insurance insurance) {
        String personId=form.getHeader().get("personId");
        JdbcTemplet jdbcTemplet = App.get("master");
        String sql="select insurance.*,\n" +
                "       case when ip.count is null then 0 else ip.count end,\n" +
                "       rewards_type.name_zh as rt_name_zh,rewards_type.name_en as rt_name_en from insurance left join (select count(1),insurance_id from insurance_person where person_id="+personId+" group by insurance_id)  ip\n" +
                "                                                                                      on insurance.insurance_id=ip.insurance_id\n" +
                "                                                                                       left join rewards_type on rewards_type.rewards_type_id=insurance.rewards_type_id";
        return jdbcTemplet.naviList(sql,form);
    }

}
