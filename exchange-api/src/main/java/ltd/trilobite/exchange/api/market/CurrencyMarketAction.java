package ltd.trilobite.exchange.api.market;

import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.AuthProxy;
import ltd.trilobite.exchange.api.BaseAction;
import ltd.trilobite.exchange.dao.CurrencyMarketDao;
import ltd.trilobite.exchange.dao.entry.CurrencyMarket;
import ltd.trilobite.exchange.service.CurrencyMarketService;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;

import java.util.UUID;

@ApiDesc(desc = "4001-OTC市场信息")
@RestService
public class CurrencyMarketAction extends BaseAction<CurrencyMarket> {

    CurrencyMarketDao currencyMarketDao = new CurrencyMarketDao();
    CurrencyMarketService currencyMarketService=new CurrencyMarketService();
    @ApiDoc(title = "新增发布信息", param = {
            @ApiDesc(name = "action", type = "string", desc = "动作"),
            @ApiDesc(name = "num", type = "string", desc = "英文名称"),
            @ApiDesc(name = "numMin", type = "string", desc = "最小单位"),
            @ApiDesc(name = "numMax", type = "string", desc = "最大单位"),
            @ApiDesc(name = "onOff", type = "string", desc = "信息状态"),
            @ApiDesc(name = "token", type = "string", desc = "依据编号"),
            @ApiDesc(name = "useCurrency", type = "string", desc = "购买货币"),
            @ApiDesc(name = "rewardsTypeId", type = "string", desc = "使用货币"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/currencyMarket/add")
    @Proxy(target = AuthProxy.class)
    public Result add(RestForm form, @Param CurrencyMarket currencyMarket) {
        Long personId=Long.parseLong(form.getHeader().get("personId"));
        currencyMarket.setToken(UUID.randomUUID().toString());
        currencyMarket.setPersonId(personId);
        currencyMarket.setOnOff(0);//待支付
        return currencyMarketService.add(currencyMarket);
    }

    @ApiDoc(title = "修改发布信息", param = {
            @ApiDesc(name = "currencyMarketId", type = "string", desc = "动作"),
            @ApiDesc(name = "action", type = "string", desc = "动作"),
            @ApiDesc(name = "num", type = "string", desc = "英文名称"),
            @ApiDesc(name = "numMin", type = "string", desc = "最小单位"),
            @ApiDesc(name = "numMax", type = "string", desc = "最大单位"),
            @ApiDesc(name = "onOff", type = "string", desc = "信息状态"),
            @ApiDesc(name = "token", type = "string", desc = "依据编号"),
            @ApiDesc(name = "useCurrency", type = "string", desc = "英文名称"),
            @ApiDesc(name = "rewardsTypeId", type = "string", desc = "英文名称"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/currencyMarket/update")
    @Proxy(target = AuthProxy.class)
    public Result update(RestForm form, @Param CurrencyMarket currencyMarket) {
        return currencyMarketService.update(currencyMarket);
    }
    @ApiDoc(title = "撤销挂单", param = {
            @ApiDesc(name = "currencyMarketId", type = "string", desc = "编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/currencyMarket/exist")
    @Proxy(target = AuthProxy.class)
    public Result exist(RestForm form, @Param CurrencyMarket currencyMarket) {
        Long personId=Long.parseLong(form.getHeader().get("personId"));
        currencyMarket.setPersonId(personId);
        return currencyMarketService.exist(currencyMarket);
    }

    @ApiDoc(title = "我要卖请求支付", param = {
            @ApiDesc(name = "currencyMarketId", type = "string", desc = "编号"),
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/currencyMarket/queryPay")
    @Proxy(target = AuthProxy.class)
    public Result queryPay(RestForm form, @Param CurrencyMarket currencyMarket) {
        Long personId=Long.parseLong(form.getHeader().get("personId"));
        currencyMarket.setPersonId(personId);
        return currencyMarketService.queryPay(currencyMarket);
    }

    @ApiDoc(title = "确认支付", param = {
            @ApiDesc(name = "key", type = "string", desc = "关键钥匙"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/currencyMarket/confirmPay")
    @Proxy(target = AuthProxy.class)
    public Result confirmPay(RestForm form, @Param CurrencyMarket currencyMarket) {

        return currencyMarketService.confirmPay(form.get("key"));
    }

    @Router(path = "/currencyMarket/findOne")
    @Proxy(target = AuthProxy.class)
    public Result findOne(RestForm form, @Param CurrencyMarket currencyMarket) {
        return super.findOne1(currencyMarketDao, currencyMarket, CurrencyMarket.class);
    }

    @ApiDoc(title = "当前货币市场列表", param = {
            @ApiDesc(name = "action", type = "header", desc = "动作 1、卖 2、买"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/currencyMarket/perList")
    @Proxy(target = AuthProxy.class)
    public Result perList(RestForm form, @Param CurrencyMarket currencyMarket) {
        return currencyMarketDao.perList1(form);
    }

}
