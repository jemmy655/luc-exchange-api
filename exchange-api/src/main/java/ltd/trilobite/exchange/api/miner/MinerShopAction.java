package ltd.trilobite.exchange.api.miner;

import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.AuthProxy;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.factory.App;
import ltd.trilobite.sdk.jdbc.JdbcTemplet;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;

@ApiDesc(desc = "2001-矿机商城(废弃)")
@RestService
public class MinerShopAction {
    @Router(path = "/shop/product/perList")
    @Proxy(target = AuthProxy.class)
    public Result perList(RestForm form) {
        JdbcTemplet jdbcTemplet = App.get("master");
        String sql="select pm.product_id,miner.*,m2.price,m2.unit_name from product_miner pm\n" +
                "  left join\n" +
                "       ( select * from member_product where product_type_id=1 and shop_id=1)\n" +
                "        m2 on pm.product_id = m2.member_product_id\n" +
                "  left join miner on pm.miner_id = miner.miner_id order by pm.product_id desc";
        return jdbcTemplet.naviList(sql,form);
    }

    @Router(path = "/shop/product/productPrice")
    @Proxy(target = AuthProxy.class)
    public Result productPrice(RestForm form) {
        JdbcTemplet jdbcTemplet = App.get("master");
        String sql="select  * from (\n" +
                "               select * from product_price where member_product_id="+form.get("memberProductId")+"\n" +
                "    ) pp left join rewards_type rt on rt.rewards_type_id=pp.rewards_type_id\n";
        return new Result(jdbcTemplet.findList(sql));
    }




    @Router(path = "/shop/checkOrder")
    @Proxy(target = AuthProxy.class)
    public Result checkOrder(RestForm form) {
        JdbcTemplet jdbcTemplet = App.get("master");
        String memberProductId=form.get("memberProductId");
        String product_price_id=form.get("productPriceId");
        String personId=form.getHeader().get("personId");
        String payPass=form.get("payPass");
        //判断支付密码是否正确，进行支付扣除

        //支付成功后入订单成功数据，入订单日志
        //订单成功后加入矿机
        jdbcTemplet.getMap("");


        String sql="select  * from (\n" +
                "               select * from product_price where member_product_id="+form.get("memberProductId")+"\n" +
                "    ) pp left join rewards_type rt on rt.rewards_type_id=pp.rewards_type_id\n";
        return new Result(jdbcTemplet.findList(sql));
    }




}
