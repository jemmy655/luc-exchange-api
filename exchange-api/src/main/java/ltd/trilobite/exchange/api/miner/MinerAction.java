package ltd.trilobite.exchange.api.miner;

import ltd.trilobite.adminproxy.AdminAuthProxy;
import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;

import ltd.trilobite.common.AuthProxy;
import ltd.trilobite.exchange.api.BaseAction;
import ltd.trilobite.exchange.dao.MinerDao;
import ltd.trilobite.exchange.dao.entry.Miner;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;

@ApiDesc(desc = "2003-矿机设置")
@RestService
public class MinerAction extends BaseAction<Miner> {

    MinerDao minerDao = new MinerDao();


    @ApiDoc(title = "新增", param = {
            @ApiDesc(name = "nameZh", type = "string", desc = "中文名称"),
            @ApiDesc(name = "nameEn", type = "string", desc = "英文名称"),
            @ApiDesc(name = "coutPower", type = "string", desc = "总能量"),
            @ApiDesc(name = "hourCountPrice", type = "string", desc = "每小时价格"),
            @ApiDesc(name = "imageUrl", type = "string", desc = "图片"),
            @ApiDesc(name = "runHour", type = "string", desc = "运行小时数"),
            @ApiDesc(name = "soreNum", type = "string", desc = "库存数量"),
            @ApiDesc(name = "buyGoup", type = "string", desc = "是否组合支付"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "200", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/miner/add")
    @Proxy(target = AdminAuthProxy.class)
    public Result add(RestForm form, @Param Miner miner) {
        miner.setOnOff(null);
        return super.add1(minerDao, miner);
    }
    @ApiDoc(title = "修改", param = {
            @ApiDesc(name = "minerId", type = "string", desc = "编号"),
            @ApiDesc(name = "nameZh", type = "string", desc = "中文名称"),
            @ApiDesc(name = "nameEn", type = "string", desc = "英文名称"),
            @ApiDesc(name = "coutPower", type = "string", desc = "总能量"),
            @ApiDesc(name = "hourCountPrice", type = "string", desc = "每小时价格"),
            @ApiDesc(name = "imageUrl", type = "string", desc = "图片"),
            @ApiDesc(name = "runHour", type = "string", desc = "运行小时数"),
            @ApiDesc(name = "soreNum", type = "string", desc = "库存数量"),
            @ApiDesc(name = "buyGoup", type = "string", desc = "是否组合支付"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "200", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/miner/update")
    @Proxy(target = AdminAuthProxy.class)
    public Result update(RestForm form, @Param Miner miner) {
        miner.setOnOff(null);
        return super.update1(minerDao, miner);
    }

    @ApiDoc(title = "删除", param = {
            @ApiDesc(name = "minerId", type = "string", desc = "编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "200", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/miner/delete")
    @Proxy(target = AdminAuthProxy.class)
    public Result delete(RestForm form, @Param Miner miner) {
        return super.delete1(minerDao, miner);
    }

    @ApiDoc(title = "上架", param = {
            @ApiDesc(name = "minerId", type = "string", desc = "编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "200", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/miner/on")
    @Proxy(target = AdminAuthProxy.class)
    public Result on(RestForm form, @Param Miner miner) {
        miner.setOnOff(1);
        return super.update1(minerDao, miner);
    }

    @ApiDoc(title = "下架", param = {
            @ApiDesc(name = "minerId", type = "string", desc = "编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "200", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/miner/off")
    @Proxy(target = AdminAuthProxy.class)
    public Result off(RestForm form, @Param Miner miner) {
        miner.setOnOff(0);
        return super.update1(minerDao, miner);
    }

    @ApiDoc(title = "查询列表", param = {
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "200", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/miner/findList")
    @Proxy(target = AdminAuthProxy.class)
    public Result findList(RestForm form, @Param Miner miner) {
        return super.findList1(minerDao, miner, Miner.class);
    }

    @ApiDoc(title = "分页查询", param = {

            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "200", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/miner/perList")
    @Proxy(target = AdminAuthProxy.class)
    public Result perList(RestForm form, @Param Miner miner) {
        return minerDao.perList(form);
    }

    @ApiDoc(title = "矿机商城列表", param = {
            @ApiDesc(name = "pageSize", type = "string", desc = "分页大小"),
            @ApiDesc(name = "start", type = "string", desc = "开始行"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "200", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/miner/mineList")
    @Proxy(target = AuthProxy.class)
    public Result mineList(RestForm form, @Param Miner miner) {
        return minerDao.mineList(form);
    }

}
