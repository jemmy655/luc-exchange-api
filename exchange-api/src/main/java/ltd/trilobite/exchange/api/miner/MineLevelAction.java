package ltd.trilobite.exchange.api.miner;

import ltd.trilobite.adminproxy.AdminAuthProxy;
import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.exchange.api.BaseAction;
import ltd.trilobite.exchange.dao.MineLevelDao;
import ltd.trilobite.exchange.dao.entry.MineLevel;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;

@ApiDesc(desc = "2008-矿机等级")
@RestService
public class MineLevelAction extends BaseAction<MineLevel> {
    MineLevelDao mineLevelDao = new MineLevelDao();

    @ApiDoc(title = "新增", param = {
            @ApiDesc(name = "mineLevelId", type = "string", desc = "编号"),
            @ApiDesc(name = "nameZh", type = "string", desc = "中文名称"),
            @ApiDesc(name = "nameEn", type = "string", desc = "英文名称"),
    },
            result = {
                    @ApiDesc(name = "200", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/mineLevel/add")
    @Proxy(target = AdminAuthProxy.class)
    public Result add(RestForm form, @Param MineLevel mineLevel) {
        return super.add1(mineLevelDao, mineLevel);
    }

    @ApiDoc(title = "修改", param = {
            @ApiDesc(name = "mineLevelId", type = "string", desc = "编号"),
            @ApiDesc(name = "nameZh", type = "string", desc = "中文名称"),
            @ApiDesc(name = "nameEn", type = "string", desc = "英文名称"),
    },
            result = {
                    @ApiDesc(name = "200", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/mineLevel/update")
    @Proxy(target = AdminAuthProxy.class)
    public Result update(RestForm form, @Param MineLevel mineLevel) {
        return super.update1(mineLevelDao, mineLevel);
    }
    @ApiDoc(title = "删除", param = {
            @ApiDesc(name = "mineLevelId", type = "string", desc = "编号"),
    },
            result = {
                    @ApiDesc(name = "200", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/mineLevel/delete")
    @Proxy(target = AdminAuthProxy.class)
    public Result delete(RestForm form, @Param MineLevel mineLevel) {
        return super.delete1(mineLevelDao, mineLevel);
    }
    @ApiDoc(title = "查询列表", param = {
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "200", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/mineLevel/findList")
    @Proxy(target = AdminAuthProxy.class)
    public Result findList(RestForm form, @Param MineLevel mineLevel) {
        return super.findList1(mineLevelDao, mineLevel, MineLevel.class);
    }
    @ApiDoc(title = "分页查询列表", param = {
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "200", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/mineLevel/perList")
    @Proxy(target = AdminAuthProxy.class)
    public Result perList(RestForm form, @Param MineLevel mineLevel) {
        return super.perList1(mineLevelDao, form, mineLevel, MineLevel.class);
    }

}
