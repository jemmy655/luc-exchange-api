package ltd.trilobite.exchange.api.miner;

import ltd.trilobite.adminproxy.AdminAuthProxy;
import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.AuthProxy;
import ltd.trilobite.exchange.api.BaseAction;
import ltd.trilobite.exchange.dao.MinerPriceDao;
import ltd.trilobite.exchange.dao.entry.MinerPrice;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;

@ApiDesc(desc = "2006-矿机价格设置")
@RestService
public class MinerPriceAction extends BaseAction<MinerPrice> {
    MinerPriceDao minerPriceDao = new MinerPriceDao();
    @ApiDoc(title = "获取头部信息", param = {

    },
            result = {
                    @ApiDesc(name = "200", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/minerPrice/getHeader")
    public Result getHeader(RestForm form, @Param MinerPrice minerPrice) {
        //form.getHeader().get("X-Real-IP")
        return new Result(form);
    }
    @ApiDoc(title = "新增", param = {
            @ApiDesc(name = "minerPriceId", type = "string", desc = "编号"),
            @ApiDesc(name = "minerId", type = "string", desc = "矿机编号"),
            @ApiDesc(name = "rewardsTypeId", type = "string", desc = "奖励货币编号"),
            @ApiDesc(name = "price", type = "string", desc = "每小时价格"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "200", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/minerPrice/add")
    @Proxy(target = AdminAuthProxy.class)
    public Result add(RestForm form, @Param MinerPrice minerPrice) {

        return super.add1(minerPriceDao, minerPrice);
    }
    @ApiDoc(title = "修改", param = {
            @ApiDesc(name = "minerPriceId", type = "string", desc = "编号"),
            @ApiDesc(name = "minerId", type = "string", desc = "矿机编号"),
            @ApiDesc(name = "rewardsTypeId", type = "string", desc = "奖励货币编号"),
            @ApiDesc(name = "price", type = "string", desc = "每小时价格"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "200", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/minerPrice/update")
    @Proxy(target = AdminAuthProxy.class)
    public Result update(RestForm form, @Param MinerPrice minerPrice) {
        minerPrice.setMinerId(null);
        minerPrice.setRewardsTypeId(null);
        return super.update1(minerPriceDao, minerPrice);
    }
    @ApiDoc(title = "删除", param = {
            @ApiDesc(name = "minerPriceId", type = "string", desc = "编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "200", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/minerPrice/delete")
    @Proxy(target = AdminAuthProxy.class)
    public Result delete(RestForm form, @Param MinerPrice minerPrice) {
        return super.delete1(minerPriceDao, minerPrice);
    }

    @ApiDoc(title = "查询列表", param = {
            @ApiDesc(name = "minerId", type = "string", desc = "编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "200", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/minerPrice/findlist")
    @Proxy(target = AdminAuthProxy.class)
    public Result findlist(RestForm form, @Param MinerPrice minerPrice) {
        return new Result(minerPriceDao.findList(minerPrice));
    }
    @ApiDoc(title = "查询分页列表", param = {
            @ApiDesc(name = "minerId", type = "string", desc = "编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "200", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/minerPrice/perList")
    @Proxy(target = AuthProxy.class)
    public Result perList(RestForm form, @Param MinerPrice minerPrice) {
        return super.perList1(minerPriceDao, form, minerPrice, MinerPrice.class);
    }

}
