package ltd.trilobite.exchange.api.miner;

import ltd.trilobite.adminproxy.AdminAuthProxy;
import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.AuthProxy;
import ltd.trilobite.exchange.api.BaseAction;
import ltd.trilobite.exchange.dao.MinerProduceDao;
import ltd.trilobite.exchange.dao.entry.MinerProduce;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;

@ApiDesc(desc = "2007-矿机产量")
@RestService
public class MinerProduceAction extends BaseAction<MinerProduce> {
    MinerProduceDao minerProduceDao = new MinerProduceDao();


    @ApiDoc(title = "新增", param = {
            @ApiDesc(name = "minerPriceId", type = "string", desc = "编号"),
            @ApiDesc(name = "minerId", type = "string", desc = "矿机编号"),
            @ApiDesc(name = "rewardsTypeId", type = "string", desc = "奖励货币编号"),
            @ApiDesc(name = "price", type = "string", desc = "每小时产出价格"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "200", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/minerProduce/add")
    @Proxy(target = AdminAuthProxy.class)
    public Result add(RestForm form, @Param MinerProduce minerProduce) {
        return super.add1(minerProduceDao, minerProduce);
    }
    @ApiDoc(title = "修改", param = {
            @ApiDesc(name = "minerPriceId", type = "string", desc = "编号"),
            @ApiDesc(name = "minerId", type = "string", desc = "矿机编号"),
            @ApiDesc(name = "rewardsTypeId", type = "string", desc = "奖励货币编号"),
            @ApiDesc(name = "price", type = "string", desc = "每小时产出价格"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "200", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/minerProduce/update")
    @Proxy(target = AdminAuthProxy.class)
    public Result update(RestForm form, @Param MinerProduce minerProduce) {
        minerProduce.setMinerId(null);
        minerProduce.setRewardsTypeId(null);
        return super.update1(minerProduceDao, minerProduce);
    }
    @ApiDoc(title = "删除", param = {
            @ApiDesc(name = "minerPriceId", type = "string", desc = "编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "200", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/minerProduce/delete")
    @Proxy(target = AdminAuthProxy.class)
    public Result delete(RestForm form, @Param MinerProduce minerProduce) {
        return super.delete1(minerProduceDao, minerProduce);
    }
    @ApiDoc(title = "查询列表", param = {
            @ApiDesc(name = "minerId", type = "string", desc = "编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "200", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/minerProduce/findlist")
    @Proxy(target = AdminAuthProxy.class)
    public Result findlist(RestForm form, @Param MinerProduce minerProduce) {
        return new Result(minerProduceDao.findList(minerProduce)) ;
    }
    @ApiDoc(title = "查询分页列表", param = {
            @ApiDesc(name = "minerId", type = "string", desc = "编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "200", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/minerProduce/perList")
    @Proxy(target = AuthProxy.class)
    public Result perList(RestForm form, @Param MinerProduce minerProduce) {
        return super.perList1(minerProduceDao, form, minerProduce, MinerProduce.class);
    }

}
