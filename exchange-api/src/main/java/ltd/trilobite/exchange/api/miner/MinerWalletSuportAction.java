package ltd.trilobite.exchange.api.miner;

import ltd.trilobite.adminproxy.AdminAuthProxy;
import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.AuthProxy;
import ltd.trilobite.exchange.api.BaseAction;
import ltd.trilobite.exchange.dao.MinerWalletSuportDao;
import ltd.trilobite.exchange.dao.entry.MinerWalletSuport;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;

@ApiDesc(desc = "2012-矿机功能钱包支持")
@RestService
public class MinerWalletSuportAction extends BaseAction<MinerWalletSuport> {
    MinerWalletSuportDao minerWalletSuportDao = new MinerWalletSuportDao();

    @ApiDoc(title = "新增", param = {
            @ApiDesc(name = "minerWalletSuportId", type = "string", desc = "编号"),
            @ApiDesc(name = "nameZh", type = "string", desc = "中文"),
            @ApiDesc(name = "nameEn", type = "string", desc = "英文"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "200", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/minerWalletSuport/add")
    @Proxy(target = AdminAuthProxy.class)
    public Result add(RestForm form, @Param MinerWalletSuport minerWalletSuport) {

        return super.add1(minerWalletSuportDao, minerWalletSuport);
    }
    @ApiDoc(title = "修改", param = {
            @ApiDesc(name = "minerWalletSuportId", type = "string", desc = "编号"),
            @ApiDesc(name = "nameZh", type = "string", desc = "中文"),
            @ApiDesc(name = "nameEn", type = "string", desc = "英文"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "200", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/minerWalletSuport/update")
    @Proxy(target = AdminAuthProxy.class)
    public Result update(RestForm form, @Param MinerWalletSuport minerWalletSuport) {
        return super.update1(minerWalletSuportDao, minerWalletSuport);
    }
    @ApiDoc(title = "删除", param = {
            @ApiDesc(name = "minerWalletSuportId", type = "string", desc = "编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "200", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/minerWalletSuport/delete")
    @Proxy(target = AdminAuthProxy.class)
    public Result delete(RestForm form, @Param MinerWalletSuport minerWalletSuport) {
        return super.delete1(minerWalletSuportDao, minerWalletSuport);
    }

    @ApiDoc(title = "查询列表", param = {
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "200", type = "string", desc = "成功"),
            }

    )

    @Router(path = "/minerWalletSuport/findList")
    @Proxy(target = AdminAuthProxy.class)
    public Result perList(RestForm form, @Param MinerWalletSuport minerWalletSuport) {
        return new Result(minerWalletSuportDao.findList(minerWalletSuport));
    }

    @ApiDoc(title = "查询购买支持钱包列表", param = {
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "200", type = "string", desc = "成功"),
            }

    )

    @Router(path = "/minerWalletSuport/buyFindList")
    @Proxy(target = AdminAuthProxy.class)
    public Result buyFindList(RestForm form, @Param MinerWalletSuport minerWalletSuport) {
        return new Result(minerWalletSuportDao.buyFindList(minerWalletSuport));
    }

}
