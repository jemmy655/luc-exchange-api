package ltd.trilobite.exchange.api.miner;

import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.AuthProxy;
import ltd.trilobite.exchange.api.BaseAction;
import ltd.trilobite.exchange.dao.MinerWalletDao;
import ltd.trilobite.exchange.dao.entry.MinerWallet;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;

@ApiDesc(desc = "2011-矿机钱包")
@RestService
public class MinerWalletAction extends BaseAction<MinerWallet> {
    MinerWalletDao minerWalletDao = new MinerWalletDao();


    @ApiDoc(title = "我的矿机价值", param = {
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/minerWallet/myAssets")
    @Proxy(target = AuthProxy.class)
    public Result findOne(RestForm form, @Param MinerWallet minerWallet) {
        Long personId = Long.parseLong(form.getHeader().get("personId"));
        return new Result(minerWalletDao.myAssets(personId));
    }

    @ApiDoc(title = "查询矿机产出钱包", param = {
            @ApiDesc(name = "personMinerId", type = "string", desc = "购买矿机的编号"),
            @ApiDesc(name = "walletSuport", type = "string", desc = "钱包类型"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/minerWallet/perList")
    @Proxy(target = AuthProxy.class)
    public Result perList(RestForm form, @Param MinerWallet minerWallet) {
        return minerWalletDao.perlist(form);
    }

}
