package ltd.trilobite.exchange.api.financial;

import ltd.trilobite.annotaion.Param;
import ltd.trilobite.annotaion.Proxy;
import ltd.trilobite.annotaion.RestService;
import ltd.trilobite.annotaion.Router;
import ltd.trilobite.common.AuthProxy;
import ltd.trilobite.exchange.api.BaseAction;
import ltd.trilobite.exchange.dao.*;
import ltd.trilobite.exchange.dao.entry.*;
import ltd.trilobite.sdk.doc.ApiDesc;
import ltd.trilobite.sdk.doc.ApiDoc;
import ltd.trilobite.sdk.status.RestForm;
import ltd.trilobite.sdk.status.Result;
import ltd.trilobite.sdk.util.Util;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.*;

@ApiDesc(desc = "3004-互助")
@RestService
public class MutualAidAction extends BaseAction<MutualAid> {
    MutualAidDao mutualAidDao = new MutualAidDao();
    ShopDao shopDao = new ShopDao();
    RewardsTypeDao rewardsTypeDao = new RewardsTypeDao();
    PayPersonDao payPersonDao = new PayPersonDao();
    BillDao billDao = new BillDao();
    MutualAidPersonDao mutualAidPersonDao=new MutualAidPersonDao();
    Map<String, Object> order = new HashMap<>();

    @ApiDoc(title = "拉取支付", param = {
            @ApiDesc(name = "mutualAidId", type = "string", desc = "互助产品编号"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "true", type = "string", desc = "成功"),
            }

    )
    @Router(path = "/mutualAid/queryPay")
    @Proxy(target = AuthProxy.class)
    public Result queryPay(RestForm form) {
        Long personId = Long.parseLong(form.getHeader().get("personId"));
        Map<String, Object> result = new HashMap<String, Object>();
        String uid = UUID.randomUUID().toString();
        String productId = form.get("mutualAidId");
        MutualAid mutualAidParam = new MutualAid();
        mutualAidParam.setMutualAidId(Long.parseLong(productId));
        MutualAid mutualAid = mutualAidDao.findOne(mutualAidParam, MutualAid.class);
        RewardsType rewardsTypeParam = new RewardsType();
        rewardsTypeParam.setRewardsTypeId(mutualAid.getRewardsTypeId());
        RewardsType rewardsType = rewardsTypeDao.findOne(rewardsTypeParam, RewardsType.class);
        result.put("mutualaid", mutualAid);
        result.put("rewardType", rewardsType);
        result.put("balance", billDao.balance(personId, mutualAid.getRewardsTypeId()));
        result.put("uid", uid);
        order.put(uid, result);
        new Thread(() -> {
            try {
                Thread.sleep(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            order.remove(uid);
        }).start();
        return new Result(result);
    }


    @ApiDoc(title = "支付", param = {
            @ApiDesc(name = "uid", type = "string", desc = "支付编号"),
            @ApiDesc(name = "num", type = "string", desc = "支付数量"),
            @ApiDesc(name = "password", type = "string", desc = "支付密码"),
            @ApiDesc(name = "token", type = "header", desc = "密匙")
    },
            result = {
                    @ApiDesc(name = "200", type = "string", desc = "成功"),
                    @ApiDesc(name = "-1", type = "string", desc = "余额不够"),
                    @ApiDesc(name = "-2", type = "string", desc = "支付超时"),
                    @ApiDesc(name = "-3", type = "string", desc = "支付密码不对"),
                    @ApiDesc(name = "-4", type = "string", desc = "捐赠期限超期"),
            }
    )
    @Router(path = "/mutualAid/checkOut")
    @Proxy(target = AuthProxy.class)
    public Result checkOut(RestForm form) {
        String uid = form.get("uid");
        Long personId = Long.parseLong(form.getHeader().get("personId"));
        Double num = 0d;
        if (Util.isNotEmpty(form.get("num"))) {
            num = Double.parseDouble(form.get("num"));
        }

        if (!order.containsKey(uid)) {
            return new Result(-2);
        }
        if (!payPersonDao.isPlayUser(form.get("password").toString(), personId)) {
            return new Result(-3);
        }



        Map<String, Object> order1 = (Map<String, Object>) order.get(uid);
        MutualAid mutualAid = (MutualAid) order1.get("mutualaid");
        if(mutualAid.getEndTime().getTime()<new Date().getTime()){
            return new Result(-4);
        }
        Double balance = (Double) order1.get("balance");
        if (balance < num) {
            return new Result(-1);
        }


        Bill bill = new Bill();
        bill.setRewardsTypeId(mutualAid.getRewardsTypeId());
        bill.setNum(-num);
        bill.setDescription("捐助");
        bill.setRewardsActionId(7);
        bill.setPersonId(personId);
        //bill.setActionCode("mutualaid");
        bill.setState(3);
        bill.setSrcId(mutualAid.getMutualAidId());
        billDao.add(bill);
        MutualAidPerson mutualAidPerson=new MutualAidPerson();
        mutualAidPerson.setPersonId(personId);
        mutualAidPerson.setMutualAidId(mutualAid.getMutualAidId());
        mutualAidPerson.setNum(num);
        mutualAidPerson.setRewardsTypeId(mutualAid.getRewardsTypeId());

        mutualAidPersonDao.add(mutualAidPerson);
        order.remove(uid);
        return new Result(200);
    }
    @Router(path = "/mutualAid/add")
    @Proxy(target = AuthProxy.class)
    public Result add(RestForm form, @Param MutualAid mutualAid) {
        return super.add1(mutualAidDao, mutualAid);
    }

    @Router(path = "/mutualAid/update")
    @Proxy(target = AuthProxy.class)
    public Result update(RestForm form, @Param MutualAid mutualAid) {
        return super.update1(mutualAidDao, mutualAid);
    }

    @Router(path = "/mutualAid/delete")
    @Proxy(target = AuthProxy.class)
    public Result delete(RestForm form, @Param MutualAid mutualAid) {
        return super.delete1(mutualAidDao, mutualAid);
    }

    @Router(path = "/mutualAid/findOne")
    @Proxy(target = AuthProxy.class)
    public Result findOne(RestForm form, @Param MutualAid mutualAid) {
        return super.findOne1(mutualAidDao, mutualAid, MutualAid.class);
    }

    @Router(path = "/mutualAid/perList")
    @Proxy(target = AuthProxy.class)
    public Result perList(RestForm form, @Param MutualAid mutualAid) {
        return mutualAidDao.perList1(form);
    }

}
